package com.lapak.lapakkite;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.lapak.lapakkite.fragment.MerchantFavFragment;
import com.lapak.lapakkite.fragment.NewsFavFragment;

public class ViewPagerAdapter extends FragmentStateAdapter {

    public ViewPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment = new Fragment();
        switch (position) {
            case 0 : {
                fragment = new NewsFavFragment();
                break;
            }
            case 1: {
                fragment = new MerchantFavFragment();
            }
        }
        return fragment;
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
