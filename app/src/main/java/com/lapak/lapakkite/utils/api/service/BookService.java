package com.lapak.lapakkite.utils.api.service;

import com.lapak.lapakkite.json.CheckStatusTransRequest;
import com.lapak.lapakkite.json.CheckStatusTransResponse;
import com.lapak.lapakkite.json.DetailRequestJson;
import com.lapak.lapakkite.json.DetailTransResponseJson;
import com.lapak.lapakkite.json.GetNearRideCarRequestJson;
import com.lapak.lapakkite.json.GetNearRideCarResponseJson;
import com.lapak.lapakkite.json.ItemRequestJson;
import com.lapak.lapakkite.json.LocationDriverRequest;
import com.lapak.lapakkite.json.LocationDriverResponse;
import com.lapak.lapakkite.json.RideCarRequestJson;
import com.lapak.lapakkite.json.RideCarResponseJson;
import com.lapak.lapakkite.json.SendRequestJson;
import com.lapak.lapakkite.json.SendResponseJson;
import com.lapak.lapakkite.json.fcm.CancelBookRequestJson;
import com.lapak.lapakkite.json.fcm.CancelBookResponseJson;
import com.lapak.lapakkite.json.fcm.DefaultResponseJson;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Ourdevelops Team on 10/17/2019.
 */

public interface BookService {

    @POST("customerapi/list_ride")
    Call<GetNearRideCarResponseJson> getNearRide(@Body GetNearRideCarRequestJson param);

    @POST("customerapi/list_car")
    Call<GetNearRideCarResponseJson> getNearCar(@Body GetNearRideCarRequestJson param);

    @POST("customerapi/request_transaksi")
    Call<RideCarResponseJson> requestTransaksi(@Body RideCarRequestJson param);

    @POST("customerapi/inserttransaksimerchant")
    Call<RideCarResponseJson> requestTransaksiMerchant(@Body ItemRequestJson param);

    @POST("customerapi/request_transaksi_send")
    Call<SendResponseJson> requestTransaksisend(@Body SendRequestJson param);

    @POST("customerapi/check_status_transaksi")
    Call<CheckStatusTransResponse> checkStatusTransaksi(@Body CheckStatusTransRequest param);

    @POST("customerapi/user_cancel")
    Call<CancelBookResponseJson> cancelOrder(@Body CancelBookRequestJson param);

    @POST("customerapi/liat_lokasi_driver")
    Call<LocationDriverResponse> liatLokasiDriver(@Body LocationDriverRequest param);

    @POST("customerapi/detail_transaksi")
    Call<DetailTransResponseJson> detailtrans(@Body DetailRequestJson param);

}
