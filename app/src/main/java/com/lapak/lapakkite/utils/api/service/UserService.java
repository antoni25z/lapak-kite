package com.lapak.lapakkite.utils.api.service;

import com.lapak.lapakkite.json.AllMerchantByNearResponseJson;
import com.lapak.lapakkite.json.AllMerchantbyCatRequestJson;
import com.lapak.lapakkite.json.AllTransResponseJson;
import com.lapak.lapakkite.json.BankResponseJson;
import com.lapak.lapakkite.json.ChangePassRequestJson;
import com.lapak.lapakkite.json.DetailRequestJson;
import com.lapak.lapakkite.json.EditprofileRequestJson;
import com.lapak.lapakkite.json.GetAllMerchantbyCatRequestJson;
import com.lapak.lapakkite.json.GetHomeRequestJson;
import com.lapak.lapakkite.json.GetHomeResponseJson;
import com.lapak.lapakkite.json.GetMerchantbyCatRequestJson;
import com.lapak.lapakkite.json.GetServiceResponseJson;
import com.lapak.lapakkite.json.LoginRequestJson;
import com.lapak.lapakkite.json.LoginResponseJson;
import com.lapak.lapakkite.json.MerchantByCatResponseJson;
import com.lapak.lapakkite.json.MerchantByIdResponseJson;
import com.lapak.lapakkite.json.MerchantByNearResponseJson;
import com.lapak.lapakkite.json.MerchantbyIdRequestJson;
import com.lapak.lapakkite.json.NewsDetailRequestJson;
import com.lapak.lapakkite.json.NewsDetailResponseJson;
import com.lapak.lapakkite.json.NotificationResponseJson;
import com.lapak.lapakkite.json.PrivacyRequestJson;
import com.lapak.lapakkite.json.PrivacyResponseJson;
import com.lapak.lapakkite.json.PromoRequestJson;
import com.lapak.lapakkite.json.PromoResponseJson;
import com.lapak.lapakkite.json.RateRequestJson;
import com.lapak.lapakkite.json.RateResponseJson;
import com.lapak.lapakkite.json.RegisterRequestJson;
import com.lapak.lapakkite.json.RegisterResponseJson;
import com.lapak.lapakkite.json.ResponseJson;
import com.lapak.lapakkite.json.SearchMerchantbyCatRequestJson;
import com.lapak.lapakkite.json.StripeRequestJson;
import com.lapak.lapakkite.json.TopupRequestJson;
import com.lapak.lapakkite.json.TopupResponseJson;
import com.lapak.lapakkite.json.TransactionInfoRequestJson;
import com.lapak.lapakkite.json.TransactionInfoResponseJson;
import com.lapak.lapakkite.json.WalletRequestJson;
import com.lapak.lapakkite.json.WalletResponseJson;
import com.lapak.lapakkite.json.WithdrawRequestJson;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Ourdevelops Team on 10/13/2019.
 */

public interface UserService {

    @POST("customerapi/login")
    Call<LoginResponseJson> login(@Body LoginRequestJson param);

    @POST("customerapi/kodepromo")
    Call<PromoResponseJson> promocode(@Body PromoRequestJson param);

    @POST("customerapi/listkodepromo")
    Call<PromoResponseJson> listpromocode(@Body PromoRequestJson param);

    @POST("customerapi/list_bank")
    Call<BankResponseJson> listbank(@Body WithdrawRequestJson param);

    @POST("customerapi/changepass")
    Call<LoginResponseJson> changepass(@Body ChangePassRequestJson param);

    @POST("customerapi/register_user")
    Call<RegisterResponseJson> register(@Body RegisterRequestJson param);

    @GET("customerapi/detail_fitur")
    Call<GetServiceResponseJson> getFitur();

    @POST("customerapi/forgot")
    Call<LoginResponseJson> forgot(@Body LoginRequestJson param);

    @POST("customerapi/privacy")
    Call<PrivacyResponseJson> privacy(@Body PrivacyRequestJson param);

    @POST("customerapi/home")
    Call<GetHomeResponseJson> home(@Body GetHomeRequestJson param);

    @POST("customerapi/topupstripe")
    Call<TopupResponseJson> topup(@Body TopupRequestJson param);

    @POST("customerapi/stripeaction")
    Call<ResponseJson> actionstripe(@Body StripeRequestJson param);

    @POST("customerapi/intentstripe")
    Call<ResponseJson> intentstripe(@Body StripeRequestJson param);

    @POST("customerapi/withdraw")
    Call<ResponseJson> withdraw(@Body WithdrawRequestJson param);

    @POST("customerapi/topuppaypal")
    Call<ResponseJson> topuppaypal(@Body WithdrawRequestJson param);

    @POST("customerapi/rate_driver")
    Call<RateResponseJson> rateDriver(@Body RateRequestJson param);

    @POST("customerapi/edit_profile")
    Call<RegisterResponseJson> editProfile(@Body EditprofileRequestJson param);

    @POST("customerapi/wallet")
    Call<WalletResponseJson> wallet(@Body WalletRequestJson param);

    @POST("customerapi/createtransaction")
    Call<TransactionInfoResponseJson> createTransaction(@Body TransactionInfoRequestJson param);

    @POST("customerapi/history_progress")
    Call<AllTransResponseJson> history(@Body DetailRequestJson param);

    @POST("customerapi/detail_berita")
    Call<NewsDetailResponseJson> beritadetail(@Body NewsDetailRequestJson param);

    @POST("customerapi/all_berita")
    Call<NewsDetailResponseJson> allberita(@Body NewsDetailRequestJson param);

    @POST("customerapi/merchantbykategoripromo")
    Call<MerchantByCatResponseJson> getmerchanbycat(@Body GetMerchantbyCatRequestJson param);

    @POST("customerapi/merchantbykategori")
    Call<MerchantByNearResponseJson> getmerchanbynear(@Body GetMerchantbyCatRequestJson param);

    @POST("customerapi/allmerchantbykategori")
    Call<AllMerchantByNearResponseJson> getallmerchanbynear(@Body GetAllMerchantbyCatRequestJson param);

    @POST("customerapi/itembykategori")
    Call<MerchantByIdResponseJson> getitembycat(@Body GetAllMerchantbyCatRequestJson param);

    @POST("customerapi/searchmerchant")
    Call<AllMerchantByNearResponseJson> searchmerchant(@Body SearchMerchantbyCatRequestJson param);

    @POST("customerapi/allmerchant")
    Call<AllMerchantByNearResponseJson> allmerchant(@Body AllMerchantbyCatRequestJson param);

    @POST("customerapi/merchantbyid")
    Call<MerchantByIdResponseJson> merchantbyid(@Body MerchantbyIdRequestJson param);

    @GET("customerapi/getcustomernotification")
    Call<NotificationResponseJson> getCustomerNotif();
}
