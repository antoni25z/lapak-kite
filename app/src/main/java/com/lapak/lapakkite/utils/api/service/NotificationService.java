package com.lapak.lapakkite.utils.api.service;

import com.lapak.lapakkite.json.fcm.CancelRequestJson;
import com.lapak.lapakkite.json.fcm.ChatRequestJson;
import com.lapak.lapakkite.json.fcm.DefaultResponseJson;
import com.lapak.lapakkite.json.fcm.RequestDriverJson;
import com.lapak.lapakkite.models.Notif;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface NotificationService {

    @POST("notificationapi/sendmobilenotification")
    Call<DefaultResponseJson> sendNotification(@Body ChatRequestJson param);

    @POST("notificationapi/sendmobilenotification")
    Call<DefaultResponseJson> sendNotification(@Body RequestDriverJson param);

    @POST("notificationapi/sendmobilenotification")
    Call<DefaultResponseJson> sendNotification(@Body CancelRequestJson param);

    @POST("notificationapi/sendmobilenotification")
    Call<DefaultResponseJson> sendNotification(@Body Notif param);
}
