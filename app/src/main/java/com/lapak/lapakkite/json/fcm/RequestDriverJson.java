package com.lapak.lapakkite.json.fcm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class RequestDriverJson {

    String token;
    Data data;

    public RequestDriverJson(final String token, final Data data) {
        this.token = token;
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("transaction_id")
        String transactionId;

        @Expose
        @SerializedName("customer_id")
        String customerId;

        @Expose
        @SerializedName("reg_id_pelanggan")
        String regIdPelanggan;

        @Expose
        @SerializedName("service_order")
        String serviceOrder;

        @Expose
        @SerializedName("price")
        long price;

        @Expose
        @SerializedName("order_time")
        Date orderTime;

        @Expose
        @SerializedName("pickup_address")
        String pickupAddress;

        @Expose
        @SerializedName("destination_address")
        String destinationAddress;

        @Expose
        @SerializedName("wallet_payment")
        String walletPayment;

        @Expose
        @SerializedName("icon")
        String icon;

        @Expose
        @SerializedName("layanan")
        String layanan;


        @Expose
        @SerializedName("layanandesc")
        String layananDesc;

        @Expose
        @SerializedName("estimate_time")
        String estimateTime;

        @Expose
        @SerializedName("cost")
        String cost;

        @Expose
        @SerializedName("admin_fee")
        long adminFee;

        @Expose
        @SerializedName("distance")
        String distance;

        @Expose
        @SerializedName("merchant_token")
        String merchantToken;


        @Expose
        @SerializedName("merchant_transaction_id")
        String merchantTransactionId;

        @Expose
        @SerializedName("type")
        int type;

        public Data(final String transactionId, final String customerId, final String regIdPelanggan, final String serviceOrder, final long price, final Date orderTime, final String pickupAddress, final String destinationAddress, final String walletPayment, final String icon, final String layanan, final String layananDesc, final String estimateTime, final String cost, final long adminFee, final String distance, final String merchantToken, final String merchantTransactionId, final int type) {
            this.transactionId = transactionId;
            this.customerId = customerId;
            this.regIdPelanggan = regIdPelanggan;
            this.serviceOrder = serviceOrder;
            this.price = price;
            this.orderTime = orderTime;
            this.pickupAddress = pickupAddress;
            this.destinationAddress = destinationAddress;
            this.walletPayment = walletPayment;
            this.icon = icon;
            this.layanan = layanan;
            this.layananDesc = layananDesc;
            this.estimateTime = estimateTime;
            this.cost = cost;
            this.adminFee = adminFee;
            this.distance = distance;
            this.merchantToken = merchantToken;
            this.merchantTransactionId = merchantTransactionId;
            this.type = type;
        }
    }
}
