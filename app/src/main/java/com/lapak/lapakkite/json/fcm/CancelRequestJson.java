package com.lapak.lapakkite.json.fcm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CancelRequestJson {
    private String token;
    private Data data;

    public CancelRequestJson(final String token, final Data data) {
        this.token = token;
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("id")
        private String id;

        @Expose
        @SerializedName("transaction_id")
        private String idTransaksi;

        @Expose
        @SerializedName("response")
        private String response;

        @Expose
        @SerializedName("type")
        public int type;

        public Data(final String idTransaksi, final String response, final int type) {
            this.idTransaksi = idTransaksi;
            this.response = response;
            this.type = type;
        }
    }
}
