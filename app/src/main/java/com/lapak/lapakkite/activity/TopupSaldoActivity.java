package com.lapak.lapakkite.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.lapak.lapakkite.R;
import com.lapak.lapakkite.constants.BaseApp;
import com.lapak.lapakkite.json.TransactionInfoRequestJson;
import com.lapak.lapakkite.json.TransactionInfoResponseJson;
import com.lapak.lapakkite.json.fcm.DefaultResponseJson;
import com.lapak.lapakkite.models.User;
import com.lapak.lapakkite.utils.Log;
import com.lapak.lapakkite.utils.SettingPreference;
import com.lapak.lapakkite.utils.Utility;
import com.lapak.lapakkite.utils.api.ServiceGenerator;
import com.lapak.lapakkite.utils.api.service.BookService;
import com.lapak.lapakkite.utils.api.service.UserService;
import com.wensolution.wensxendit.PaymentMethod;
import com.wensolution.wensxendit.WensXendit;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopupSaldoActivity extends AppCompatActivity {
    EditText nominal;
    ImageView text1, text2, text3, text4;
    RelativeLayout rlnotif, rlprogress;
    TextView textnotif;
    String disableback;
    private String paymentAmount;
    SettingPreference sp;
    ImageView backBtn;
    boolean debug;

    ActivityResultLauncher<Intent> paymentLauncher;

    WensXendit wensXendit;
    Button choosePm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);
        sp = new SettingPreference(this);

        nominal = findViewById(R.id.balance);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text4 = findViewById(R.id.text4);
        rlnotif = findViewById(R.id.rlnotif);
        textnotif = findViewById(R.id.textnotif);
        rlprogress = findViewById(R.id.rlprogress);
        choosePm = findViewById(R.id.choose_pm_btn);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        wensXendit = new WensXendit(this);
        wensXendit.setXenditApiKey(sp.getSetting()[15]);
        wensXendit.setActiveMethods(new String[]{
                PaymentMethod.BRI,
                PaymentMethod.MANDIRI,
                PaymentMethod.BSI,
                PaymentMethod.PERMATA,
                PaymentMethod.BNI,
                PaymentMethod.BJB,
                PaymentMethod.DANA,
                PaymentMethod.OVO,
                PaymentMethod.SHOPEEPAY,
                PaymentMethod.LINKAJA,
                PaymentMethod.ASTRAPAY,
                PaymentMethod.QRIS,
        });

        nominal.addTextChangedListener(Utility.currencyTW(nominal, this));

        choosePm.setOnClickListener(v -> createTransaction());

        text1.setOnClickListener(v -> nominal.setText("20000"));

        text2.setOnClickListener(v -> nominal.setText("50000"));

        text3.setOnClickListener(v -> nominal.setText("100000"));

        text4.setOnClickListener(v -> nominal.setText("200000"));

        disableback = "false";
    }


    public void notif(String text) {
        rlnotif.setVisibility(View.VISIBLE);
        textnotif.setText(text);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                rlnotif.setVisibility(View.GONE);
            }
        }, 3000);
    }


    @Override
    public void onBackPressed() {
        if (!disableback.equals("true")) {
            finish();
        }
    }

    private void createTransaction() {
        if (!nominal.getText().toString().isEmpty()) {
            progressshow();
            String referenceId = "pm-level-" + UUID.randomUUID();
            paymentAmount = nominal.getText().toString();
            final User user = BaseApp.getInstance(this).getLoginUser();
            TransactionInfoRequestJson request = new TransactionInfoRequestJson();
            request.setReferenceId(referenceId);
            request.setUserId(user.getId());
            request.setName(user.getFullnama());
            request.setTypeUser("customer");
            request.setAmount(convertAngka(paymentAmount));

            UserService service = ServiceGenerator.createService(UserService.class, user.getNoTelepon(), user.getPassword());
            service.createTransaction(request).enqueue(new Callback<TransactionInfoResponseJson>() {
                @Override
                public void onResponse(Call<TransactionInfoResponseJson> call, Response<TransactionInfoResponseJson> response) {
                    progresshide();
                    if (response.isSuccessful()) {
                        wensXendit.startPayment(
                                Long.parseLong(convertAngka(paymentAmount)),
                                response.body().getData().getReferenceId(),
                                response.body().getData().getName()
                        );
                    } else {
                        notif("error");
                    }
                }

                @Override
                public void onFailure(Call<TransactionInfoResponseJson> call, Throwable t) {
                    progresshide();
                    notif(t.getMessage());
                    t.printStackTrace();
                }
            });
        } else {
            notif("Nominal Kosong !");
        }
    }

    public void progressshow() {
        rlprogress.setVisibility(View.VISIBLE);
        disableback = "true";
    }

    public void progresshide() {
        rlprogress.setVisibility(View.GONE);
        disableback = "false";
    }

    public String convertAngka(String value) {
        return (((((value + "")
                .replaceAll(sp.getSetting()[0], ""))
                .replaceAll(" ", ""))
                .replaceAll(",", ""))
                .replaceAll("[Rp.]", ""));
    }


}
