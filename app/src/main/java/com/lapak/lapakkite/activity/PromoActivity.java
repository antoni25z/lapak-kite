package com.lapak.lapakkite.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.lapak.lapakkite.R;
import com.lapak.lapakkite.constants.BaseApp;
import com.lapak.lapakkite.item.PromoItem;
import com.lapak.lapakkite.json.PromoRequestJson;
import com.lapak.lapakkite.json.PromoResponseJson;
import com.lapak.lapakkite.models.User;
import com.lapak.lapakkite.utils.api.ServiceGenerator;
import com.lapak.lapakkite.utils.api.service.UserService;

import java.util.Objects;

public class PromoActivity extends AppCompatActivity {

    ShimmerFrameLayout shimmer;
    RecyclerView recycle;
    PromoItem promoItem;
    RelativeLayout rlnodata;
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_promo);
        this.shimmer = this.findViewById(R.id.shimmer);
        this.recycle = this.findViewById(R.id.inboxlist);
        this.rlnodata = this.findViewById(R.id.rlnodata);
        this.backButton = this.findViewById(R.id.back_btn);
       // this.recycle.setHasFixedSize(true);
        this.recycle.setNestedScrollingEnabled(false);
        this.recycle.setLayoutManager(new GridLayoutManager(this, 1));
        this.backButton.setOnClickListener(v -> this.finish());
        this.getData();
    }

    private void shimmershow() {
        this.rlnodata.setVisibility(View.GONE);
        this.recycle.setVisibility(View.GONE);
        this.shimmer.setVisibility(View.VISIBLE);
        this.shimmer.startShimmer();
    }

    private void shimmertutup() {

        this.recycle.setVisibility(View.VISIBLE);
        this.shimmer.setVisibility(View.GONE);
        this.shimmer.stopShimmer();
    }

    private void getData() {
        shimmershow();
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        UserService service = ServiceGenerator.createService(UserService.class, loginUser.getEmail(), loginUser.getPassword());
        PromoRequestJson param = new PromoRequestJson();

        service.listpromocode(param).enqueue(new Callback<PromoResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<PromoResponseJson> call, @NonNull Response<PromoResponseJson> response) {
                if (response.isSuccessful()) {
                    shimmertutup();
                    if (Objects.requireNonNull(response.body()).getData().isEmpty()) {
                        rlnodata.setVisibility(View.VISIBLE);
                    } else {
                        promoItem = new PromoItem(PromoActivity.this, response.body().getData(), R.layout.item_promo);
                        recycle.setAdapter(promoItem);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<PromoResponseJson> call, @NonNull Throwable t) {

            }
        });

    }
}