package com.lapak.lapakkite.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.lapak.lapakkite.databinding.ActivityWithdrawBinding;
import com.lapak.lapakkite.item.BanksAdapter;
import com.lapak.lapakkite.item.BanksClick;
import com.lapak.lapakkite.utils.Log;
import com.lapak.lapakkite.utils.SettingPreference;
import com.wensolution.wensxendit.AvailableBankModel;
import com.wensolution.wensxendit.WensXendit;
import com.wensolution.wensxendit.apiservice.requestbody.ValidateNameRequestBody;

import java.util.ArrayList;
import java.util.Objects;

import androidx.recyclerview.widget.LinearLayoutManager;

public class WithdrawActivity extends AppCompatActivity implements BanksClick {
    SettingPreference sp;
    private ActivityWithdrawBinding binding;

    BanksAdapter adapter;
    String bankName,bankCode, disburseCode;

    boolean disableback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityWithdrawBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        sp = new SettingPreference(this);

        progressshow();

        disableback = false;

        WensXendit wensXendit = new WensXendit(this);
        wensXendit.setIlumaApiKey(sp.getSetting()[19]);
        wensXendit.setXenditApiKey(sp.getSetting()[15]);

        binding.banksRv.setLayoutManager(new LinearLayoutManager(this));

        wensXendit.getAvailableBanks(availableBankModels -> {
            Log.d("2504", "size : " + availableBankModels.size());
            adapter = new BanksAdapter((ArrayList<AvailableBankModel>) availableBankModels, this);
            binding.banksRv.setAdapter(adapter);
            progresshide();
            return null;
        });

        binding.chooseBankEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isBlank()) {
                    binding.banksCard.setVisibility(View.GONE);
                } else {
                    binding.banksCard.setVisibility(View.VISIBLE);
                }
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.nextBtn.setOnClickListener(view -> {
            progressshow();
            String bankNumber = Objects.requireNonNull(binding.withdrawNumberEdt.getText()).toString();

            ValidateNameRequestBody validateNameRequestBody = new ValidateNameRequestBody(
                    bankNumber,
                    bankCode
            );

            if (bankNumber.isBlank() || bankCode.isBlank() || Objects.requireNonNull(binding.chooseBankEdt.getText()).toString().isBlank()) {
                notif("Fill the field");
            } else {
                wensXendit.validateBankName(validateNameRequestBody, result -> {
                    progresshide();
                    if (result.getFound()) {
                        Intent intent = new Intent(this, ConfirmWithdrawActivity.class);
                        intent.putExtra("holder_name", result.getHolderName());
                        intent.putExtra("bank_name", result.getBankName());
                        intent.putExtra("bank_number", bankNumber);
                        intent.putExtra("disburse_code", disburseCode);
                        startActivity(intent);
                    } else {
                        notif(result.getMessage());
                    }
                    return null;
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!disableback) {
            finish();
        }

    }

    public void notif(String text) {
        binding.rlnotif.setVisibility(View.VISIBLE);
        binding.textnotif.setText(text);

        new Handler().postDelayed(() -> binding.rlnotif.setVisibility(View.GONE), 3000);
    }


    public void progressshow() {
        binding.rlprogress.setVisibility(View.VISIBLE);
        disableback = true;
    }

    public void progresshide() {
        binding.rlprogress.setVisibility(View.GONE);
        disableback = true;
    }

    @Override
    public void onClick(AvailableBankModel availableBankModel) {
        bankName = availableBankModel.getName();
        bankCode = availableBankModel.getCodeIluma();
        disburseCode = availableBankModel.getCodeDisburse();

        binding.chooseBankEdt.setText(bankName);

        binding.banksCard.setVisibility(View.GONE);
    }
}
