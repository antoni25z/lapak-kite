package com.lapak.lapakkite.activity;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.lapak.lapakkite.R;
import com.lapak.lapakkite.constants.BaseApp;
import com.lapak.lapakkite.constants.Constant;
import com.lapak.lapakkite.databinding.ActivityRateBinding;
import com.lapak.lapakkite.json.DetailRequestJson;
import com.lapak.lapakkite.json.DetailTransResponseJson;
import com.lapak.lapakkite.json.RateRequestJson;
import com.lapak.lapakkite.json.RateResponseJson;
import com.lapak.lapakkite.models.DriverModel;
import com.lapak.lapakkite.models.User;
import com.lapak.lapakkite.utils.api.ServiceGenerator;
import com.lapak.lapakkite.utils.api.service.BookService;
import com.lapak.lapakkite.utils.api.service.UserService;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RateActivity<RatingView> extends AppCompatActivity {

    String iddriver, idtrans, response, submit;

    private ActivityRateBinding binding;

    public RateActivity() {
    }

    public static <RatingView> RateActivity<RatingView> createRateActivity() {
        return new RateActivity<RatingView>();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRateBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Intent intent = getIntent();
        iddriver = intent.getStringExtra("driver_id");
        idtrans = intent.getStringExtra("transaction_id");
        response = intent.getStringExtra("response");
        getData(idtrans, iddriver);
        submit = "true";
        shimmeractive();
        removeNotif();
    }

    private void shimmeractive() {
        binding.shimmername.startShimmer();
    }

    private void shimmernonactive() {
        binding.shimmername.setVisibility(View.GONE);
        binding.image.setVisibility(View.VISIBLE);
        binding.namadriver.setVisibility(View.VISIBLE);
        binding.addComment.setVisibility(View.VISIBLE);
        binding.submit.setVisibility(View.VISIBLE);
        //ratingview.setVisibility(View.VISIBLE);
        binding.shimmername.stopShimmer();
    }

    private void getData(String idtrans, String iddriver) {
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        BookService service = ServiceGenerator.createService(BookService.class, loginUser.getEmail(), loginUser.getPassword());
        DetailRequestJson param = new DetailRequestJson();
        param.setId(idtrans);
        param.setIdDriver(iddriver);
        service.detailtrans(param).enqueue(new Callback<DetailTransResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<DetailTransResponseJson> call, @NonNull Response<DetailTransResponseJson> response) {
                if (response.isSuccessful()) {
                    DriverModel driver = Objects.requireNonNull(response.body()).getDriver().get(0);
                    parsedata(driver);

                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<DetailTransResponseJson> call, @NonNull Throwable t) {

            }
        });


    }

    private void parsedata(final DriverModel driver) {

        Picasso.get()
                .load(Constant.IMAGESDRIVER + driver.getFoto())
                .placeholder(R.drawable.image_placeholder)
                .into(binding.image);
        binding.namadriver.setText(driver.getNamaDriver());
        final User userLogin = BaseApp.getInstance(this).getLoginUser();
        binding.ratingView.setRating(0);
        if (submit.equals("true")) {
            binding.submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RateRequestJson request = new RateRequestJson();
                    request.transaction_id = idtrans;
                    request.customer_id = userLogin.getId();
                    request.driver_id = iddriver;
                    request.rating = String.valueOf(binding.ratingView.getRating());
                    request.note = binding.addComment.getText().toString();
                    ratingUser(request);

                }
            });
        }
        shimmernonactive();
    }

    private void ratingUser(RateRequestJson request) {
        submit = "false";
        binding.submit.setText(getString(R.string.waiting_pleaseWait));
        binding.submit.setBackground(getResources().getDrawable(R.drawable.rounded_corners_button));

        User loginUser = BaseApp.getInstance(RateActivity.this).getLoginUser();

        UserService service = ServiceGenerator.createService(UserService.class, loginUser.getEmail(), loginUser.getPassword());
        service.rateDriver(request).enqueue(new Callback<RateResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<RateResponseJson> call, @NonNull Response<RateResponseJson> response) {
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).mesage.equals("success")) {
                        Intent i = new Intent(RateActivity.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    }
                }
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFailure(@NonNull Call<RateResponseJson> call, @NonNull Throwable t) {
                t.printStackTrace();
                submit = "true";
                binding.submit.setText("Submit");
                binding.submit.setBackground(getResources().getDrawable(R.drawable.button_round_1));
            }
        });


    }

    private void removeNotif() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Objects.requireNonNull(notificationManager).cancel(0);
    }


}
