package com.lapak.lapakkite.activity;

import static com.lapak.lapakkite.activity.PicklocationActivity.LOCATION_LATLNG;
import static com.lapak.lapakkite.json.fcm.FCMType.ORDER;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lapak.lapakkite.R;
import com.lapak.lapakkite.constants.BaseApp;
import com.lapak.lapakkite.item.ItemItem;
import com.lapak.lapakkite.json.CheckStatusTransRequest;
import com.lapak.lapakkite.json.CheckStatusTransResponse;
import com.lapak.lapakkite.json.GetNearRideCarRequestJson;
import com.lapak.lapakkite.json.GetNearRideCarResponseJson;
import com.lapak.lapakkite.json.ItemRequestJson;
import com.lapak.lapakkite.json.PromoRequestJson;
import com.lapak.lapakkite.json.PromoResponseJson;
import com.lapak.lapakkite.json.RideCarResponseJson;
import com.lapak.lapakkite.json.fcm.DefaultResponseJson;
import com.lapak.lapakkite.json.fcm.DriverRequest;
import com.lapak.lapakkite.json.fcm.DriverResponse;
import com.lapak.lapakkite.json.fcm.RequestDriverJson;
import com.lapak.lapakkite.models.DriverModel;
import com.lapak.lapakkite.models.ItemModel;
import com.lapak.lapakkite.models.PesananMerchant;
import com.lapak.lapakkite.models.ServiceModel;
import com.lapak.lapakkite.models.TransModel;
import com.lapak.lapakkite.models.User;
import com.lapak.lapakkite.utils.Log;
import com.lapak.lapakkite.utils.SettingPreference;
import com.lapak.lapakkite.utils.Utility;
import com.lapak.lapakkite.utils.api.ServiceGenerator;
import com.lapak.lapakkite.utils.api.service.BookService;
import com.lapak.lapakkite.utils.api.service.NotificationService;
import com.lapak.lapakkite.utils.api.service.UserService;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.MapboxDirections;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.listeners.ClickEventHook;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class DetailOrderActivity extends AppCompatActivity implements ItemItem.OnCalculatePrice {

    TextView location, orderprice, deliveryfee, diskon, total, diskontext, topuptext, textnotif, saldotext;
    Button order;
    RecyclerView rvmerchantnear;
    LinearLayout llcheckedcash, llcheckedwallet, llbtn;
    ImageButton checkedcash, checkedwallet;
    RelativeLayout rlprogress;
    Thread thread;
    boolean threadRun = true;
    TransModel transaksi;
    private DriverRequest request;
    TextView cashpayment, walletpayment;
    private double jarak;
    private long diskonVoucher;
    public static final String FITUR_KEY = "FiturKey";
    String alamat, biayadistance;
    double lat, lon, merlat, merlon, distance;
    private ServiceModel designedFitur;
    private final int DESTINATION_ID = 1;

    private FastItemAdapter<ItemItem> itemAdapter;
    private Realm realm;
    ImageView backbtn;
    private List<DriverModel> driverAvailable;
    private long foodCostLong = 0, maksimum;
    private String saldoWallet;
    private String checkedpaycash;
    private String idresto;
    private String alamatresto;
    private String namamerchant;
    private String back;
    int service;
    SettingPreference sp;
    RelativeLayout rlnotif;
    String time;

    EditText promokode;
    String home, layanan, description, icon;
    LatLng pickUpLatLang, destinationLatLang;
    Point pickup, destination;

    Button btnpromo;
    
    TextView fee;

    long minimumKm, adminFee;

    long totalBiaya;
    long subBiaya;
    long ongkir;
    long biaya;
    long biayaMinimum;

    double diskonDompet;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);
        diskonVoucher = 0;
        time = "0 mins";
        realm = BaseApp.getInstance(this).getRealmInstance();
        rvmerchantnear = findViewById(R.id.merchantnear);
        location = findViewById(R.id.pickUpText);
        orderprice = findViewById(R.id.orderprice);
        fee = findViewById(R.id.fee);
        llcheckedcash = findViewById(R.id.llcheckedcash);
        llcheckedwallet = findViewById(R.id.llcheckedwallet);
        cashpayment = findViewById(R.id.cashPayment);
        walletpayment = findViewById(R.id.walletpayment);
        deliveryfee = findViewById(R.id.cost);
        checkedcash = findViewById(R.id.checkedcash);
        checkedwallet = findViewById(R.id.checkedwallet);
        total = findViewById(R.id.price);
        diskon = findViewById(R.id.diskon);
        backbtn = findViewById(R.id.back_btn);
        diskontext = findViewById(R.id.ketsaldo);
        topuptext = findViewById(R.id.topUp);
        order = findViewById(R.id.order);
        rlprogress = findViewById(R.id.rlprogress);
        textnotif = findViewById(R.id.textnotif);
        rlnotif = findViewById(R.id.rlnotif);
        saldotext = findViewById(R.id.balance);
        promokode = findViewById(R.id.promocode);
        btnpromo = findViewById(R.id.btnpromo);
        back = "0";
        llbtn = findViewById(R.id.llbtn);

        driverAvailable = new ArrayList<>();
        service = 0;
        topuptext.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), TopupSaldoActivity.class)));

        location.setOnClickListener(v -> {
            Intent intent = new Intent(DetailOrderActivity.this, PicklocationActivity.class);
            intent.putExtra(PicklocationActivity.FORM_VIEW_INDICATOR, DESTINATION_ID);
            startActivityForResult(intent, PicklocationActivity.LOCATION_PICKER_ID);
        });

        backbtn.setOnClickListener(view -> finish());

        sp = new SettingPreference(this);
        Intent intent = getIntent();
        lat = intent.getDoubleExtra("lat", 0);
        lon = intent.getDoubleExtra("lon", 0);
        merlat = intent.getDoubleExtra("merlat", 0);
        merlon = intent.getDoubleExtra("merlon", 0);
        distance = intent.getDoubleExtra("distance", 0);
        alamatresto = intent.getStringExtra("alamatresto");
        idresto = intent.getStringExtra("idresto");
        alamat = intent.getStringExtra("alamat");
        namamerchant = intent.getStringExtra("namamerchant");
        service = intent.getIntExtra(FITUR_KEY, -1);
        if (service != -1)
            designedFitur = realm.where(ServiceModel.class).equalTo("idFitur", service).findFirst();

        home = Objects.requireNonNull(designedFitur).getHome();
        layanan = designedFitur.getFitur();
        description = designedFitur.getKeterangan();
        icon = designedFitur.getIcon();

        biaya = designedFitur.getBiaya();
        biayaMinimum = designedFitur.getBiaya_minimum();
        diskonDompet = designedFitur.getBiayaAkhir();
        minimumKm = designedFitur.getMinimumCostDistance();
        adminFee = designedFitur.getAdminFee();


        maksimum = Long.parseLong(designedFitur.getMaksimumdist());

        diskontext.setText("Discount " + designedFitur.getDiskon() + " with Wallet");
        total.setText("wait");
        deliveryfee.setText("wait");
        Utility.currencyTXT(diskon, String.valueOf(diskonVoucher), DetailOrderActivity.this);
        User userLogin = BaseApp.getInstance(this).getLoginUser();
        saldoWallet = String.valueOf(userLogin.getWalletSaldo());
        pickUpLatLang = new LatLng(merlat, merlon);
        destinationLatLang = new LatLng(lat, lon);
        pickup = Point.fromLngLat(merlon, merlat);
        destination = Point.fromLngLat(lon, lat);
        location.setText(alamat);

        Utility.currencyTXT(diskon, String.valueOf(diskonVoucher), DetailOrderActivity.this);

        long saldokini = Long.parseLong(saldoWallet);

        llcheckedcash.setOnClickListener(view -> {
            long hasilDiskon = subBiaya - diskonVoucher;
            totalBiaya = hasilDiskon + adminFee;

            Utility.currencyTXT(total, String.valueOf(totalBiaya), DetailOrderActivity.this);
            Utility.currencyTXT(diskon, String.valueOf(diskonVoucher), DetailOrderActivity.this);
            checkedcash.setSelected(true);
            checkedwallet.setSelected(false);
            checkedpaycash = "1";
            cashpayment.setTextColor(getResources().getColor(R.color.colorgradient));
            walletpayment.setTextColor(getResources().getColor(R.color.gray));
            checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
            checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
        });
        llcheckedwallet.setOnClickListener(view -> {
            long hasilDiskon = (long) (subBiaya * diskonDompet);
            long totalDiskon = hasilDiskon + diskonVoucher;

            if (Long.parseLong(saldoWallet) >= ((subBiaya - totalDiskon) + adminFee)) {
                totalBiaya = (subBiaya - totalDiskon) + adminFee;
                Utility.currencyTXT(diskon, String.valueOf(totalDiskon), this);
                Utility.currencyTXT(total, String.valueOf(totalBiaya), this);

                checkedcash.setSelected(false);
                checkedwallet.setSelected(true);
                checkedpaycash = "0";
                walletpayment.setTextColor(getResources().getColor(R.color.colorgradient));
                cashpayment.setTextColor(getResources().getColor(R.color.gray));
                checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
                checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
            } else {
                notif("Saldo Tidak Cukup");
            }
        });

        btnpromo.setOnClickListener(v -> {
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                Objects.requireNonNull(imm).hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
            } catch (Exception ignored) {

            }
            if (promokode.getText().toString().isEmpty()) {
                notif("Promo code cant be empty!");
            } else {
                promokodedata();
            }
        });

        itemAdapter = new FastItemAdapter<>();
        itemAdapter.notifyDataSetChanged();
        itemAdapter.addEventHook(new ClickEventHook<ItemItem>() {
            @Nullable
            @Override
            public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                if (viewHolder instanceof ItemItem.ViewHolder) {
                    return ((ItemItem.ViewHolder) viewHolder).itemView;
                }
                return null;
            }

            @Override
            public void onClick(View v, int position, FastAdapter<ItemItem> fastAdapter, ItemItem item) {

            }
        });

        rvmerchantnear.setLayoutManager(new LinearLayoutManager(this));
        rvmerchantnear.setAdapter(itemAdapter);
        getRoute();
        updateEstimatedItemCost();
        loadItem();
    }

    public void notif(String text) {
        rlnotif.setVisibility(View.VISIBLE);
        textnotif.setText(text);

        new Handler().postDelayed(() -> rlnotif.setVisibility(View.GONE), 3000);
    }

    private void getRoute() {
        if (pickup != null && destination != null) {
            rlprogress.setVisibility(View.VISIBLE);
            MapboxDirections client = MapboxDirections.builder()
                    .origin(pickup)
                    .destination(destination)
                    .overview(DirectionsCriteria.OVERVIEW_FULL)
                    .profile(DirectionsCriteria.PROFILE_DRIVING_TRAFFIC)
                    .accessToken(getString(R.string.mapbox_access_token))
                    .build();
            client.enqueueCall(new Callback<DirectionsResponse>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<DirectionsResponse> call, @NonNull Response<DirectionsResponse> response) {
                    if (response.body() == null) {
                        Timber.d("No routes found, make sure you set the right user and access token.");
                        return;
                    } else if (response.body().routes().size() < 1) {
                        Timber.d("No routes found");
                        return;
                    }
                    rlprogress.setVisibility(View.GONE);
                    DirectionsRoute currentroute = response.body().routes().get(0);
                    String format = String.format(Locale.US, "%.0f", currentroute.distance() / 1000f);
                    long minutes = (long) ((currentroute.duration() / 60));
                    time = minutes + " mins";
                    distance = (currentroute.distance()) / 1000f;
                    updateDistance();
                    long dist = Long.parseLong(format);
                    if (dist < maksimum) {
                        order.setOnClickListener(v -> {
                            if (readyToOrder()) {
                                sendOrder();
                                back = "1";
                            }
                        });
                    } else {
                        notif("tujuan terlalu jauh!");
                        order.setOnClickListener(v -> {
                            if (readyToOrder()) {
                                notif("tujuan terlalu jauh!");
                                back = "0";
                            }
                        });

                    }

                }

                @Override
                public void onFailure(@NonNull Call<DirectionsResponse> call, @NonNull Throwable throwable) {
                    Timber.d("Error: %s", throwable.getMessage());

                }
            });
        }
    }

    private boolean readyToOrder() {
        if (destinationLatLang == null) {
            Toast.makeText(this, "Pilih lokasi anda terlebih dahulu.", Toast.LENGTH_SHORT).show();
            return false;
        } else if (total.getText().toString().isEmpty() || total.getText().toString().equals("wait")) {
            Toast.makeText(this, "Silakan Tunggu...", Toast.LENGTH_SHORT).show();
            return false;
        }

        List<PesananMerchant> existingFood = realm.copyFromRealm(realm.where(PesananMerchant.class).findAll());

        int quantity = 0;
        for (int p = 0; p < existingFood.size(); p++) {
            quantity += existingFood.get(p).getQty();
        }

        if (quantity == 0) {
            Toast.makeText(this, "Silahkan pesan minimal 1 item.", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (jarak == -99.0) {
            Toast.makeText(this, "Silakan tunggu beberapa saat...", Toast.LENGTH_SHORT).show();
        }

        return true;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PicklocationActivity.LOCATION_PICKER_ID) {
            if (resultCode == RESULT_OK) {
                String addressset = data.getStringExtra(PicklocationActivity.LOCATION_NAME);
                location.setText(addressset);

                destinationLatLang = data.getParcelableExtra(LOCATION_LATLNG);

                destination = Point.fromLngLat(destinationLatLang.getLongitude(), destinationLatLang.getLatitude());
                if (pickUpLatLang != null) {
                    getRoute();
                }

            }
        }

    }

    private void loadItem() {
        List<ItemModel> makananList = realm.copyFromRealm(realm.where(ItemModel.class).findAll());
        List<PesananMerchant> pesananFoods = realm.copyFromRealm(realm.where(PesananMerchant.class).findAll());
        itemAdapter.clear();
        for (PesananMerchant pesanan : pesananFoods) {
            ItemItem makananItem = new ItemItem(this, this);
            for (ItemModel makanan : makananList) {
                if (makanan.getId_item() == pesanan.getIdItem()) {
                    makananItem.quantity = pesanan.getQty();
                    makananItem.id = makanan.getId_item();
                    makananItem.namaMenu = makanan.getNama_item();
                    makananItem.deskripsiMenu = makanan.getDeskripsi_item();
                    makananItem.photo = makanan.getFoto_item();
                    makananItem.price = Long.parseLong(makanan.getHarga_item());
                    makananItem.promo = makanan.getStatus_promo();
                    if (makanan.getHarga_promo().isEmpty()) {
                        makananItem.hargapromo = 0;
                    } else {
                        makananItem.hargapromo = Long.parseLong(makanan.getHarga_promo());
                    }
                    makananItem.note = pesanan.getCatatan();

                    break;
                }
            }

            itemAdapter.add(makananItem);
        }

        itemAdapter.notifyDataSetChanged();
    }

    @SuppressLint("SetTextI18n")
    private void promokodedata() {
        btnpromo.setEnabled(false);
        btnpromo.setText("Tunggu...");
        final User user = BaseApp.getInstance(this).getLoginUser();
        PromoRequestJson request = new PromoRequestJson();
        request.setFitur(String.valueOf(service));
        request.setCode(promokode.getText().toString());

        UserService service = ServiceGenerator.createService(UserService.class, user.getNoTelepon(), user.getPassword());
        service.promocode(request).enqueue(new Callback<PromoResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<PromoResponseJson> call, @NonNull Response<PromoResponseJson> response) {
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("success")) {
                        btnpromo.setEnabled(true);
                        btnpromo.setText("Use");
                        if (response.body().getType().equals("persen")) {
                            diskonVoucher = (Long.parseLong(response.body().getNominal()) * subBiaya) / 100;
                        } else {
                            diskonVoucher = Long.parseLong(response.body().getNominal());
                        }
                        updateDistance();
                    } else {
                        notif("kode promo tidak tersedia!");
                        btnpromo.setEnabled(true);
                        btnpromo.setText("Use");
                        diskonVoucher = 0;
                        updateDistance();
                    }
                } else {
                    notif("error!");
                }
            }

            @Override
            public void onFailure(@NonNull Call<PromoResponseJson> call, @NonNull Throwable t) {
                t.printStackTrace();
                notif("error");
            }
        });
    }

    @Override
    public void calculatePrice() {
        updateEstimatedItemCost();
    }

    private void updateEstimatedItemCost() {
        List<PesananMerchant> existingFood = realm.copyFromRealm(realm.where(PesananMerchant.class).findAll());
        long cost = 0;
        for (int p = 0; p < existingFood.size(); p++) {
            cost += existingFood.get(p).getTotalHarga();
        }
        foodCostLong = cost;
        Utility.currencyTXT(orderprice, String.valueOf(foodCostLong), this);
        updateDistance();
    }


    double km;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (back.equals("1")) {
            Intent intent = new Intent(DetailOrderActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
    }

    private void updateDistance() {

        Log.d("2504", String.valueOf(destinationLatLang.getLatitude()));

        checkedpaycash = "1";
        checkedcash.setSelected(true);
        checkedwallet.setSelected(false);
        cashpayment.setTextColor(getResources().getColor(R.color.colorgradient));
        walletpayment.setTextColor(getResources().getColor(R.color.gray));
        checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
        checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
        km = distance;


        this.jarak = km;


        if (km > minimumKm) {
            long biayaPerKm = (long) (biaya * (km-minimumKm));
            ongkir = biayaMinimum + biayaPerKm;
        } else {
            ongkir = biayaMinimum;
        }
        subBiaya = ongkir + foodCostLong;
        totalBiaya = subBiaya  + adminFee ;
        Utility.currencyTXT(total, String.valueOf(totalBiaya), this);
        Utility.currencyTXT(fee, String.valueOf(adminFee), this);
        Utility.currencyTXT(deliveryfee, String.valueOf(ongkir), this);
        Utility.currencyTXT(diskon, String.valueOf(diskonVoucher), this);
    }

    private void sendOrder() {
        List<PesananMerchant> existingItem = realm.copyFromRealm(realm.where(PesananMerchant.class).findAll());

        for (PesananMerchant pesanan : existingItem) {
            if (pesanan.getCatatan() == null || pesanan.getCatatan().trim().equals(""))
                pesanan.setCatatan("");
        }

        ItemRequestJson param = new ItemRequestJson();
        User userLogin = BaseApp.getInstance(this).getLoginUser();
        param.setIdPelanggan(userLogin.getId());
        param.setOrderFitur(String.valueOf(service));
        param.setStartLatitude(merlat);
        param.setStartLongitude(merlon);
        param.setEndLatitude(destinationLatLang.getLatitude());
        param.setEndLongitude(destinationLatLang.getLongitude());
        param.setAlamatTujuan(location.getText().toString());
        param.setAlamatAsal(alamatresto);
        param.setJarak(jarak);
        param.setEstimasi(time);
        param.setHarga(subBiaya);
        param.setAdminFee(adminFee);
        if (checkedpaycash.equals("1")) {
            param.setPakaiWallet(0);
            param.setKreditpromo(String.valueOf(diskonVoucher));
        } else {
            param.setPakaiWallet(1);
            param.setKreditpromo(String.valueOf((subBiaya * diskonDompet) + diskonVoucher));
        }
        param.setIdResto(idresto);
        param.setTotalBiayaBelanja(foodCostLong);
        param.setCatatan("");
        param.setPesanan(existingItem);


        fetchNearDriver(param);
    }

    private void fetchNearDriver(final ItemRequestJson paramdata) {
        rlprogress.setVisibility(View.VISIBLE);
        if (destinationLatLang != null) {
            User loginUser = BaseApp.getInstance(this).getLoginUser();

            BookService services = ServiceGenerator.createService(BookService.class, loginUser.getEmail(), loginUser.getPassword());
            GetNearRideCarRequestJson param = new GetNearRideCarRequestJson();
            param.setLatitude(merlat);
            param.setLongitude(merlon);
            param.setFitur(String.valueOf(service));

            services.getNearRide(param).enqueue(new Callback<GetNearRideCarResponseJson>() {
                @Override
                public void onResponse(@NonNull Call<GetNearRideCarResponseJson> call, @NonNull Response<GetNearRideCarResponseJson> response) {
                    if (response.isSuccessful()) {
                        driverAvailable = Objects.requireNonNull(response.body()).getData();
                        if (driverAvailable.isEmpty()) {
                            finish();
                            Toast.makeText(DetailOrderActivity.this, "tidak ada pengemudi di sekitar Anda!", Toast.LENGTH_SHORT).show();
                        } else {
                            sendRequestTransaksi(paramdata, driverAvailable);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull retrofit2.Call<GetNearRideCarResponseJson> call, @NonNull Throwable t) {


                }
            });

        }
    }

    private void buildDriverRequest(RideCarResponseJson response) {
        transaksi = response.getData().get(0);
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        if (request == null) {
            request = new DriverRequest();
            request.setIdTransaksi(transaksi.getId());
            request.setIdPelanggan(transaksi.getIdPelanggan());
            request.setRegIdPelanggan(loginUser.getToken());
            request.setOrderFitur(home);
            request.setStartLatitude(transaksi.getStartLatitude());
            request.setStartLongitude(transaksi.getStartLongitude());
            request.setEndLatitude(transaksi.getEndLatitude());
            request.setEndLongitude(transaksi.getEndLongitude());
            request.setJarak(transaksi.getJarak());
            request.setHarga(totalBiaya);
            request.setAdminFee(transaksi.getAdminFee());
            request.setWaktuOrder(transaksi.getWaktuOrder());
            request.setAlamatAsal(transaksi.getAlamatAsal());
            request.setAlamatTujuan(transaksi.getAlamatTujuan());
            request.setKodePromo(transaksi.getKodePromo());
            request.setKreditPromo(transaksi.getKreditPromo());
            request.setPakaiWallet(String.valueOf(transaksi.isPakaiWallet()));
            request.setEstimasi(namamerchant);
            request.setLayanan(layanan);
            request.setLayanandesc(description);
            request.setIcon(icon);
            request.setBiaya(String.valueOf(foodCostLong));
            request.setTokenmerchant(transaksi.getToken_merchant());
            request.setIdtransmerchant(transaksi.getIdtransmerchant());
            request.setDistance(String.valueOf(ongkir));


            String namaLengkap = String.format("%s", loginUser.getFullnama());
            request.setNamaPelanggan(namaLengkap);
            request.setTelepon(loginUser.getNoTelepon());
            request.setType(ORDER);
        }
    }

    private void sendRequestTransaksi(ItemRequestJson param, final List<DriverModel> driverList) {
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        final BookService service = ServiceGenerator.createService(BookService.class, loginUser.getEmail(), loginUser.getPassword());

        service.requestTransaksiMerchant(param).enqueue(new Callback<RideCarResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<RideCarResponseJson> call, @NonNull Response<RideCarResponseJson> response) {
                if (response.isSuccessful()) {
                    buildDriverRequest(Objects.requireNonNull(response.body()));
                    thread = new Thread(() -> {
                        for (int i = 0; i < driverList.size(); i++) {
                            fcmBroadcast(driverList.get(i));
                        }

                        try {
                            Thread.sleep(30000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (threadRun) {
                            CheckStatusTransRequest param1 = new CheckStatusTransRequest();
                            param1.setIdTransaksi(transaksi.getId());
                            service.checkStatusTransaksi(param1).enqueue(new Callback<CheckStatusTransResponse>() {
                                @Override
                                public void onResponse(@NonNull Call<CheckStatusTransResponse> call1, @NonNull Response<CheckStatusTransResponse> response1) {
                                    if (response1.isSuccessful()) {
                                        CheckStatusTransResponse checkStatus = response1.body();
                                        if (!Objects.requireNonNull(checkStatus).isStatus()) {
                                            notif("Driver not found!");
                                            runOnUiThread(() -> notif("Driver not found!"));

                                            new Handler().postDelayed(DetailOrderActivity.this::finish, 3000);
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(@NonNull Call<CheckStatusTransResponse> call1, @NonNull Throwable t) {
                                    notif("Pengemudi tidak ditemukan!");
                                    runOnUiThread(() -> notif("Pengemudi tidak ditemukan!"));

                                    new Handler().postDelayed(DetailOrderActivity.this::finish, 3000);

                                }
                            });
                        }

                    });
                    thread.start();


                }
            }

            @Override
            public void onFailure(@NonNull Call<RideCarResponseJson> call, @NonNull Throwable t) {
                t.printStackTrace();
                notif("Akun Anda bermasalah, silakan hubungi layanan pelanggan!");
                new Handler().postDelayed(() -> finish(), 3000);
            }
        });
    }

    private void fcmBroadcast(DriverModel driverToSend) {
        request.setTime_accept(new Date().getTime() + "");

        User loginUser = BaseApp.getInstance(this).getLoginUser();
        final NotificationService service = ServiceGenerator.createService(NotificationService.class, loginUser.getEmail(), loginUser.getPassword());

        RequestDriverJson.Data data = new RequestDriverJson.Data(
                request.getIdTransaksi(),
                request.getIdPelanggan(),
                request.getRegIdPelanggan(),
                request.getOrderFitur(),
                request.getHarga(),
                request.getWaktuOrder(),
                request.getAlamatAsal(),
                request.getAlamatTujuan(),
                request.isPakaiWallet(),
                request.getIcon(),
                request.getLayanan(),
                request.getLayanandesc(),
                request.getEstimasi(),
                request.getBiaya(),
                request.getAdminFee(),
                request.getDistance(),
                request.getTokenmerchant(),
                request.getIdtransmerchant(),
                1
        );

        RequestDriverJson requestDriverJson = new RequestDriverJson(driverToSend.getRegId(), data);

        service.sendNotification(requestDriverJson).enqueue(new Callback<DefaultResponseJson>() {
            @Override
            public void onResponse(Call<DefaultResponseJson> call, Response<DefaultResponseJson> response) {
                Log.d("response_23", response.message());
            }

            @Override
            public void onFailure(Call<DefaultResponseJson> call, Throwable t) {
                Log.d("response_23", t.getMessage());
            }
        });
    }

    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final DriverResponse response) {
        if (response.getResponse().equalsIgnoreCase(DriverResponse.ACCEPT) || response.getResponse().equals("3") || response.getResponse().equals("4")) {
            runOnUiThread(() -> {
                threadRun = false;
                for (DriverModel cDriver : driverAvailable) {
                    if (cDriver.getId().equals(response.getId())) {
                        Intent intent = new Intent(DetailOrderActivity.this, ProgressActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("driver_id", cDriver.getId());
                        intent.putExtra("transaction_id", request.getIdTransaksi());
                        intent.putExtra("response", "2");
                        intent.putExtra("complete", "1");
                        startActivity(intent);
                        DriverResponse response1 = new DriverResponse();
                        response1.setId("");
                        response1.setIdTransaksi("");
                        response1.setResponse("");
                        EventBus.getDefault().postSticky(response1);
                        finish();
                    }
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        User userLogin = BaseApp.getInstance(this).getLoginUser();
        saldoWallet = String.valueOf(userLogin.getWalletSaldo());
        Utility.currencyTXT(saldotext, saldoWallet, this);
    }


}
