package com.lapak.lapakkite.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.Shimmer;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.lapak.lapakkite.R;
import com.lapak.lapakkite.constants.BaseApp;
import com.lapak.lapakkite.item.NotifAdapter;
import com.lapak.lapakkite.json.NotificationResponseJson;
import com.lapak.lapakkite.models.User;
import com.lapak.lapakkite.utils.Log;
import com.lapak.lapakkite.utils.api.ServiceGenerator;
import com.lapak.lapakkite.utils.api.service.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {

    ShimmerFrameLayout shimmer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        RecyclerView notifRv = findViewById(R.id.notif_rv);
        RelativeLayout noData = findViewById(R.id.rlnodata);
        shimmer = findViewById(R.id.shimmerwallet);

        shimmershow();

        notifRv.setLayoutManager(new LinearLayoutManager(this));

        User loginUser = BaseApp.getInstance(this).getLoginUser();
        UserService userService = ServiceGenerator.createService(UserService.class, loginUser.getNoTelepon(), loginUser.getPassword());

        userService.getCustomerNotif().enqueue(new Callback<NotificationResponseJson>() {
            @Override
            public void onResponse(Call<NotificationResponseJson> call, Response<NotificationResponseJson> response) {
                shimmertutup();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        if (response.body().getNotification().isEmpty()) {
                            noData.setVisibility(View.VISIBLE);
                            notifRv.setVisibility(View.GONE);
                        } else {
                            notifRv.setVisibility(View.VISIBLE);
                            noData.setVisibility(View.GONE);
                            notifRv.setAdapter(new NotifAdapter(response.body().getNotification()));
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationResponseJson> call, Throwable t) {
                Log.d("2504", t.getMessage());
            }
        });

    }

    private void shimmershow() {
        shimmer.setVisibility(View.VISIBLE);
        shimmer.startShimmer();
    }

    private void shimmertutup() {

        shimmer.setVisibility(View.GONE);
        shimmer.stopShimmer();
    }
}