package com.lapak.lapakkite.activity;

import static com.lapak.lapakkite.json.fcm.FCMType.ORDER;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconRotate;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.lapak.lapakkite.R;
import com.lapak.lapakkite.constants.BaseApp;
import com.lapak.lapakkite.constants.Constant;
import com.lapak.lapakkite.databinding.ActivityRentBinding;
import com.lapak.lapakkite.json.CheckStatusTransRequest;
import com.lapak.lapakkite.json.CheckStatusTransResponse;
import com.lapak.lapakkite.json.GetNearRideCarRequestJson;
import com.lapak.lapakkite.json.GetNearRideCarResponseJson;
import com.lapak.lapakkite.json.PromoRequestJson;
import com.lapak.lapakkite.json.PromoResponseJson;
import com.lapak.lapakkite.json.RideCarRequestJson;
import com.lapak.lapakkite.json.RideCarResponseJson;
import com.lapak.lapakkite.json.fcm.DefaultResponseJson;
import com.lapak.lapakkite.json.fcm.DriverRequest;
import com.lapak.lapakkite.json.fcm.DriverResponse;
import com.lapak.lapakkite.json.fcm.RequestDriverJson;
import com.lapak.lapakkite.models.DriverModel;
import com.lapak.lapakkite.models.ServiceModel;
import com.lapak.lapakkite.models.TransModel;
import com.lapak.lapakkite.models.User;
import com.lapak.lapakkite.utils.Log;
import com.lapak.lapakkite.utils.Utility;
import com.lapak.lapakkite.utils.api.ServiceGenerator;
import com.lapak.lapakkite.utils.api.service.BookService;
import com.lapak.lapakkite.utils.api.service.NotificationService;
import com.lapak.lapakkite.utils.api.service.UserService;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.location.LocationEngineResult;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.core.exceptions.ServicesException;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Ourdevelops Team on 10/26/2019.
 */

public class RentCarActivity extends AppCompatActivity implements OnMapReadyCallback {
    public static final String FITUR_KEY = "FiturKey";
    String ICONFITUR;
    TransModel transaksi;
    Thread thread;
    boolean threadRun = true;
    private DriverRequest request;
    Context context = this;

    private ActivityRentBinding binding;
    private List<DriverModel> driverAvailable;
    private Realm realm;
    private ServiceModel designedFitur;
    private double distance;
    private long price, promocode;
    private String saldoWallet;
    private String checkedpaywallet;
    String service, getbiaya, biayaminimum, biayaakhir,icondrver;
    MapView mapView;
    LatLng pickUpLatLang;

    String totalbiaya;

    long adminFee;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));
        super.onCreate(savedInstanceState);
        binding = ActivityRentBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        BottomSheetBehavior behavior = BottomSheetBehavior.from(binding.bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        promocode = 0;
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);


        binding.pickUpContainer.setVisibility(View.VISIBLE);

        User userLogin = BaseApp.getInstance(this).getLoginUser();
        saldoWallet = String.valueOf(userLogin.getWalletSaldo());


        binding.backBtn.setOnClickListener(view -> finish());

        binding.topUp.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), TopupSaldoActivity.class)));

        binding.order.setOnClickListener(v -> {
            if (binding.pickUpText.getText().toString().isEmpty()) {
                notif("Lokasi tidak boleh kosong!");
            } else {
                onOrderButton();
            }
        });
//
        binding.pickUpText.setOnClickListener(v -> {
            binding.pickUpContainer.setVisibility(View.VISIBLE);
            openAutocompleteActivity();
        });



        realm = BaseApp.getInstance(context).getRealmInstance();

        Intent intent = getIntent();
        int fiturId = intent.getIntExtra(FITUR_KEY, -1);
        ICONFITUR = intent.getStringExtra("icon");
        if (fiturId != -1)
            designedFitur = realm.where(ServiceModel.class).equalTo("idFitur", fiturId).findFirst();


        service = String.valueOf(Objects.requireNonNull(designedFitur).getIdFitur());
        getbiaya = String.valueOf(designedFitur.getBiaya());
        biayaminimum = String.valueOf(designedFitur.getBiaya_minimum());
        biayaakhir = String.valueOf(designedFitur.getBiayaAkhir());
        adminFee = designedFitur.getAdminFee();
        icondrver = designedFitur.getIcon_driver();
        binding.layanan.setText(designedFitur.getFitur());
        binding.layanandes.setText(designedFitur.getKeterangan());
        updateDistance(1);

        binding.adminFeeTxt.setText(String.valueOf(adminFee));

        binding.ketsaldo.setText("Discount " + designedFitur.getDiskon() + " with Wallet");
        Picasso.get()
                .load(Constant.IMAGESFITUR + ICONFITUR)
                .placeholder(R.drawable.lapak_circle)
                .into(binding.image);


        binding.btnpromo.setOnClickListener(v -> {
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                Objects.requireNonNull(imm).hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
            } catch (Exception ignored) {

            }
            if (binding.promocode.getText().toString().isEmpty()) {
                notif("Kode Promo tidak boleh kosong!");
            } else {
                promokodedata();
            }
        });

        binding.durasiEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                updateDistance(Long.parseLong(charSequence.toString()));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.minBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long day = Long.parseLong(binding.durasiEdt.getText().toString());
                if (day != 1) {
                    long l = day - 1;
                    binding.durasiEdt.setText(String.valueOf(l));
                    updateDistance(l);
                }
            }
        });

        binding.addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long day = Long.parseLong(binding.durasiEdt.getText().toString());
                long l = day + 1;
                binding.durasiEdt.setText(String.valueOf(l));
                updateDistance(l);
            }
        });

    }

    private void openAutocompleteActivity() {
        Intent intent = new PlaceAutocomplete.IntentBuilder()
                .accessToken(Mapbox.getAccessToken() != null ? Mapbox.getAccessToken() : getString(R.string.mapbox_access_token))
                .placeOptions(PlaceOptions.builder()
                        .backgroundColor(Color.parseColor("#EEEEEE"))
                        .geocodingTypes("poi", "neighborhood", "locality")
                        .country(new Locale("in", "ID"))
                        .limit(10)
                        .build(PlaceOptions.MODE_CARDS))
                .build(RentCarActivity.this);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);
                if (mapboxMap != null) {
                    binding.pickUpText.setText(selectedCarmenFeature.placeName());
                    Style style = mapboxMap.getStyle();
                    if (style != null) {
                        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                                new CameraPosition.Builder()
                                        .target(new LatLng(((Point) Objects.requireNonNull(selectedCarmenFeature.geometry())).latitude(),
                                                ((Point) selectedCarmenFeature.geometry()).longitude()))
                                        .zoom(15)
                                        .build()), 4);
                        LatLng centerPos = new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                ((Point) selectedCarmenFeature.geometry()).longitude());
                        onPickUp(style, mapboxMap, centerPos);
                    }
                }
            }
        }

    }

    @SuppressLint("SetTextI18n")
    private void promokodedata() {
        binding.btnpromo.setEnabled(false);
        binding.btnpromo.setText("Wait...");
        final User user = BaseApp.getInstance(this).getLoginUser();
        PromoRequestJson request = new PromoRequestJson();
        request.setFitur(service);
        request.setCode(binding.promocode.getText().toString());

        UserService service = ServiceGenerator.createService(UserService.class, user.getNoTelepon(), user.getPassword());
        service.promocode(request).enqueue(new Callback<PromoResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<PromoResponseJson> call, @NonNull Response<PromoResponseJson> response) {
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("success")) {
                        binding.btnpromo.setEnabled(true);
                        binding.btnpromo.setText("Use");
                        if (response.body().getType().equals("persen")) {
                            promocode = (Long.parseLong(response.body().getNominal()) * price) / 100;
                        } else {
                            promocode = Long.parseLong(response.body().getNominal());
                        }
                        if (checkedpaywallet.equals("1")) {
                            long diskonwallet = (long) (Double.parseDouble(biayaakhir) * price);
                            String diskontotal = String.valueOf(diskonwallet + promocode);
                            String totalbiaya = String.valueOf((price - (diskonwallet + promocode))+adminFee);
                            Utility.currencyTXT(binding.price, totalbiaya, context);
                            Utility.currencyTXT(binding.diskon, diskontotal, RentCarActivity.this);
                        } else {
                            String diskontotal = String.valueOf(promocode);
                            String totalbiaya = String.valueOf((price - promocode) + adminFee);
                            Utility.currencyTXT(binding.price, totalbiaya, context);
                            Utility.currencyTXT(binding.diskon, diskontotal, RentCarActivity.this);
                        }
                    } else {
                        binding.btnpromo.setEnabled(true);
                        binding.btnpromo.setText("Use");
                        notif("promo code not available!");
                        promocode = 0;
                        if (checkedpaywallet.equals("1")) {
                            long diskonwallet = (long) (Double.parseDouble(biayaakhir) * price);
                            String diskontotal = String.valueOf(diskonwallet + promocode);
                            String totalbiaya = String.valueOf((price - (diskonwallet + promocode))+adminFee);
                            Utility.currencyTXT(binding.price, totalbiaya, context);
                            Utility.currencyTXT(binding.diskon, diskontotal, RentCarActivity.this);
                        } else {
                            String diskontotal = String.valueOf(promocode);
                            String totalbiaya = String.valueOf((price - promocode) +adminFee);
                            Utility.currencyTXT(binding.price, totalbiaya, context);
                            Utility.currencyTXT(binding.diskon, diskontotal, RentCarActivity.this);
                        }
                    }
                } else {
                    notif("error!");
                }
            }

            @Override
            public void onFailure(@NonNull Call<PromoResponseJson> call, @NonNull Throwable t) {
                t.printStackTrace();
                notif("error");
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void updateDistance(long jam) {
        BottomSheetBehavior behavior = BottomSheetBehavior.from(binding.bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        binding.detail.setVisibility(View.VISIBLE);
        binding.order.setVisibility(View.VISIBLE);
        binding.service.setText(jam + " hr");
        Utility.currencyTXT(binding.diskon, String.valueOf(promocode), RentCarActivity.this);
        checkedpaywallet = "0";
        distance = jam;
        binding.checkedcash.setSelected(true);
        binding.checkedwallet.setSelected(false);
        binding.cashPayment.setTextColor(getResources().getColor(R.color.colorgradient));
        binding.walletpayment.setTextColor(getResources().getColor(R.color.gray));
        binding.checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
        binding.checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
        String costs = String.valueOf(biayaminimum);
        long biayaTotal = (long) (Double.parseDouble(getbiaya) * jam);
        this.price = biayaTotal;
        Utility.currencyTXT(binding.cost, String.valueOf(price), this);

        final long finalBiayaTotal = biayaTotal;
        totalbiaya = String.valueOf((finalBiayaTotal - promocode)+adminFee);
        Utility.currencyTXT(binding.price, totalbiaya, this);

        long saldokini = Long.parseLong(saldoWallet);
        if (saldokini < (biayaTotal - (price * Double.parseDouble(biayaakhir)))) {
            binding.llcheckedcash.setOnClickListener(view -> {
                String totalbiaya13 = String.valueOf((finalBiayaTotal - promocode) + adminFee);
                Utility.currencyTXT(binding.price, totalbiaya13, getBaseContext());
                Utility.currencyTXT(binding.diskon, String.valueOf(promocode), RentCarActivity.this);
                binding.checkedcash.setSelected(true);
                binding.checkedwallet.setSelected(false);
                checkedpaywallet = "0";
                binding.cashPayment.setTextColor(getResources().getColor(R.color.colorgradient));
                binding.walletpayment.setTextColor(getResources().getColor(R.color.gray));
                binding.checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
                binding.checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
            });
        } else {
            binding.llcheckedcash.setOnClickListener(view -> {
                String totalbiaya1 = String.valueOf((finalBiayaTotal - promocode) + adminFee);
                Utility.currencyTXT(binding.price, totalbiaya1, context);
                Utility.currencyTXT(binding.diskon, String.valueOf(promocode), RentCarActivity.this);
                binding.checkedcash.setSelected(true);
                binding.checkedwallet.setSelected(false);
                checkedpaywallet = "0";
                binding.cashPayment.setTextColor(getResources().getColor(R.color.colorgradient));
                binding.walletpayment.setTextColor(getResources().getColor(R.color.gray));
                binding.checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
                binding.checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
            });

            final long finalBiayaTotal1 = biayaTotal;
            binding.llcheckedwallet.setOnClickListener(view -> {
                long diskonwallet = (long) (Double.parseDouble(biayaakhir) * price);
                String totalwallet = String.valueOf(diskonwallet + promocode);
                Utility.currencyTXT(binding.diskon, totalwallet, context);
                String totalbiaya12 = String.valueOf((finalBiayaTotal1 - (diskonwallet + promocode)) + adminFee);
                Utility.currencyTXT(binding.price, totalbiaya12, context);
                binding.checkedcash.setSelected(false);
                binding.checkedwallet.setSelected(true);
                checkedpaywallet = "1";
                binding.walletpayment.setTextColor(getResources().getColor(R.color.colorgradient));
                binding.cashPayment.setTextColor(getResources().getColor(R.color.gray));
                binding.checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
                binding.checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
            });
        }

    }

    public void notif(String text) {
        binding.rlnotif.setVisibility(View.VISIBLE);
        binding.textnotif.setText(text);

        new Handler().postDelayed(() -> binding.rlnotif.setVisibility(View.GONE), 3000);
    }

    MapboxMap mapboxMap;
    LocationComponent locationComponent;
    Point pickup;
    @SuppressLint({"MissingPermission", "WrongConstant"})
    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = Objects.requireNonNull(lm).isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ignored) {}

        try {
            network_enabled = Objects.requireNonNull(lm).isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ignored) {}


            if (PermissionsManager.areLocationPermissionsGranted(this) | gps_enabled && network_enabled) {
                this.mapboxMap = mapboxMap;
                mapboxMap.setStyle(Constant.CUSTOM_MAPBOX_STYLE, style -> {

                    markerRide(style);
                    UiSettings uiSettings = mapboxMap.getUiSettings();
                    uiSettings.setAttributionEnabled(false);
                    uiSettings.setLogoEnabled(false);
                    uiSettings.setCompassEnabled(false);
                    uiSettings.setRotateGesturesEnabled(false);
                    locationComponent = mapboxMap.getLocationComponent();
                    locationComponent.activateLocationComponent(
                            LocationComponentActivationOptions
                                    .builder(RentCarActivity.this, style)
                                    .useDefaultLocationEngine(true)
                                    .locationEngineRequest(new LocationEngineRequest.Builder(750)
                                            .setFastestInterval(750)
                                            .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                                            .build())
                                    .build());

                    locationComponent.setLocationComponentEnabled(true);
                    locationComponent.setRenderMode(RenderMode.COMPASS);

                    locationComponent.getLocationEngine().getLastLocation(new LocationEngineCallback<LocationEngineResult>() {
                        @Override
                        public void onSuccess(LocationEngineResult result) {
                            if (result != null) {
                                CameraPosition position = new CameraPosition.Builder()
                                        .target(new LatLng(result.getLastLocation().getLatitude(), result.getLastLocation().getLongitude()))
                                        .zoom(18)
                                        .tilt(20)
                                        .build();
                                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 100);
                                fetchNearDriver(locationComponent.getLastKnownLocation().getLatitude(), locationComponent.getLastKnownLocation().getLongitude(), service, style);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            exception.printStackTrace();
                        }
                    });


                    initDroppedMarkerpickup(style);
                    binding.currentlocation.setOnClickListener(view -> {
                        locationComponent.getLocationEngine().getLastLocation(new LocationEngineCallback<LocationEngineResult>() {
                            @Override
                            public void onSuccess(LocationEngineResult result) {
                                CameraPosition position = new CameraPosition.Builder()
                                        .target(new LatLng(result.getLastLocation().getLatitude(), result.getLastLocation().getLongitude()))
                                        .zoom(18)
                                        .tilt(20)
                                        .build();
                                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 1000);
                            }

                            @Override
                            public void onFailure(@NonNull Exception exception) {

                            }
                        });

                    });
                    binding.pickUpButton.setOnClickListener(view -> {
                        LatLng centerPos = mapboxMap.getCameraPosition().target;
                        onPickUp(style, mapboxMap, centerPos);
                    });

                });
            } else {
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
    }

    private void markerRide(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addSource(new GeoJsonSource("driverid"));
        loadedMapStyle.addLayer(new SymbolLayer("layerid",
                "driverid").withProperties(
                iconImage("imageid"),
                iconAllowOverlap(true),
                visibility(Property.NONE),
                iconSize(1.0f),
                iconIgnorePlacement(true)
        ));


    }

    private void initDroppedMarkerpickup(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("dropped-icon-image-pickup", BitmapFactory.decodeResource(
                getResources(), R.drawable.ic_pikup_map));
        loadedMapStyle.addSource(new GeoJsonSource("dropped-marker-source-id-pickup"));
        loadedMapStyle.addLayer(new SymbolLayer("DROPPED_MARKER_LAYER_ID_PICKUP",
                "dropped-marker-source-id-pickup").withProperties(
                iconImage("dropped-icon-image-pickup"),
                iconAllowOverlap(true),
                visibility(Property.NONE),
                iconSize(2.0f),
                iconIgnorePlacement(true)
        ));

    }

    private void onPickUp(Style style, MapboxMap mapboxMap, LatLng centerPos) {

        style.removeLayer("layerid");
        style.removeSource("driverid");
        markerRide(style);

        fetchNearDriver(centerPos.getLatitude(), centerPos.getLongitude(), service, style);
        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(centerPos.getLatitude(), centerPos.getLongitude()))
                .zoom(15)
                .tilt(20)
                .build();
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 1000);
        binding.pickUpContainer.setVisibility(View.GONE);
        pickUpLatLang = new LatLng(centerPos.getLatitude(), centerPos.getLongitude());
        if (style.getLayer("DROPPED_MARKER_LAYER_ID_PICKUP") != null) {
            GeoJsonSource source = style.getSourceAs("dropped-marker-source-id-pickup");
            if (source != null) {
                source.setGeoJson(Point.fromLngLat(centerPos.getLongitude(), centerPos.getLatitude()));
            }
            Layer pickUpMarker = style.getLayer("DROPPED_MARKER_LAYER_ID_PICKUP");
            if (pickUpMarker != null) {
                pickUpMarker.setProperties(visibility(Property.VISIBLE));
            }
        }
        binding.textprogress.setVisibility(View.VISIBLE);

        pickup = Point.fromLngLat(centerPos.getLongitude(), centerPos.getLatitude());
        getaddress(pickup, mapboxMap, binding.pickUpText);
    }

    private void getaddress(final Point point, MapboxMap mapboxMap, TextView textView) {
        try {
            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(getString(R.string.mapbox_access_token))
                    .query(Point.fromLngLat(point.longitude(), point.latitude()))
                    .build();

            client.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(@NonNull Call<GeocodingResponse> call, @NonNull Response<GeocodingResponse> response) {

                    if (response.body() != null) {
                        List<CarmenFeature> results = response.body().features();
                        if (results.size() > 0) {
                            CarmenFeature feature = results.get(0);
                            mapboxMap.getStyle(style -> textView.setText(feature.placeName()));

                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GeocodingResponse> call, @NonNull Throwable throwable) {
                }
            });
        } catch (ServicesException servicesException) {
            Timber.e("Error geocoding: %s", servicesException.toString());
            servicesException.printStackTrace();
        }
    }

    private void fetchNearDriver(double latitude, double longitude, String service, Style style) {
        if (driverAvailable != null) {
            driverAvailable.clear();
        }

        if (mapboxMap != null) {
            User loginUser = BaseApp.getInstance(this).getLoginUser();

            BookService services = ServiceGenerator.createService(BookService.class, loginUser.getEmail(), loginUser.getPassword());
            GetNearRideCarRequestJson param = new GetNearRideCarRequestJson();
            param.setLatitude(latitude);
            param.setLongitude(longitude);
            param.setFitur(service);

            services.getNearRide(param).enqueue(new Callback<GetNearRideCarResponseJson>() {
                @Override
                public void onResponse(@NonNull Call<GetNearRideCarResponseJson> call, @NonNull Response<GetNearRideCarResponseJson> response) {
                    if (response.isSuccessful()) {
                        driverAvailable = Objects.requireNonNull(response.body()).getData();
                        createMarker(style);
                    }
                }

                @Override
                public void onFailure(@NonNull retrofit2.Call<GetNearRideCarResponseJson> call, @NonNull Throwable t) {

                }
            });
        }
    }

    List<Feature> symbolLayerIconFeatureList;
    private void createMarker(Style style) {
        Layer markermap;
        switch (icondrver) {
            case "1":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    style.addImage("imageid", BitmapFactory.decodeResource(
                            getResources(), R.drawable.drivermap));
                    markermap = style.getLayer("layerid");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs("driverid");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }
                }
                break;
            case "2":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    style.addImage("imageid", BitmapFactory.decodeResource(
                            getResources(), R.drawable.carmap));
                    markermap = style.getLayer("layerid");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs("driverid");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }
                }
                break;
            case "3":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    style.addImage("imageid", BitmapFactory.decodeResource(
                            getResources(), R.drawable.truck));
                    markermap = style.getLayer("layerid");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs("driverid");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }
                }
                break;
            case "4":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    style.addImage("imageid", BitmapFactory.decodeResource(
                            getResources(), R.drawable.delivery));
                    markermap = style.getLayer("layerid");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs("driverid");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }
                }
                break;
            case "5":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    style.addImage("imageid", BitmapFactory.decodeResource(
                            getResources(), R.drawable.hatchback));
                    markermap = style.getLayer("layerid");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs("driverid");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }
                }
                break;
            case "6":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    style.addImage("imageid", BitmapFactory.decodeResource(
                            getResources(), R.drawable.suv));
                    markermap = style.getLayer("layerid");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs("driverid");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }
                }
                break;
            case "7":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    style.addImage("imageid", BitmapFactory.decodeResource(
                            getResources(), R.drawable.van));
                    markermap = style.getLayer("layerid");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs("driverid");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }
                }
                break;
            case "8":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    style.addImage("imageid", BitmapFactory.decodeResource(
                            getResources(), R.drawable.bicycle));
                    markermap = style.getLayer("layerid");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs("driverid");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }
                }
                break;
            case "9":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    style.addImage("imageid", BitmapFactory.decodeResource(
                            getResources(), R.drawable.bajaj));
                    markermap = style.getLayer("layerid");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs("driverid");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }
                }
                break;
        }
    }


    private void onOrderButton() {
        if (checkedpaywallet.equals("1")) {
            if (driverAvailable.isEmpty()) {
                notif("Maaf, tidak ada pengemudi di sekitar Anda.");
            } else {
                RideCarRequestJson param = new RideCarRequestJson();
                User userLogin = BaseApp.getInstance(this).getLoginUser();
                param.setIdPelanggan(userLogin.getId());
                param.setOrderFitur(service);
                param.setStartLatitude(pickUpLatLang.getLatitude());
                param.setStartLongitude(pickUpLatLang.getLongitude());
                param.setEndLatitude(0);
                param.setEndLongitude(0);
                param.setJarak(distance);
                param.setHarga(this.price);
                param.setAdminFee(adminFee);
                param.setEstimasi(binding.service.getText().toString());
                param.setKreditpromo(String.valueOf((Double.parseDouble(biayaakhir) * this.price) + promocode));
                param.setAlamatAsal(binding.pickUpText.getText().toString());
                param.setAlamatTujuan("Rent Car");
                param.setPakaiWallet(1);
                sendRequestTransaksi(param, driverAvailable);
            }
        } else {
            if (driverAvailable.isEmpty()) {
                notif("Maaf, tidak ada pengemudi di sekitar Anda.");
            } else {
                RideCarRequestJson param = new RideCarRequestJson();
                User userLogin = BaseApp.getInstance(this).getLoginUser();
                param.setIdPelanggan(userLogin.getId());
                param.setOrderFitur(service);
                param.setStartLatitude(pickUpLatLang.getLatitude());
                param.setStartLongitude(pickUpLatLang.getLongitude());
                param.setEndLatitude(0);
                param.setEndLongitude(0);
                param.setJarak(distance);
                param.setHarga(this.price);
                param.setAdminFee(adminFee);
                param.setEstimasi(binding.service.getText().toString());
                param.setKreditpromo(String.valueOf(promocode));
                param.setAlamatAsal(binding.pickUpText.getText().toString());
                param.setAlamatTujuan("Rent Car");
                param.setPakaiWallet(0);

                sendRequestTransaksi(param, driverAvailable);
            }
        }
    }

    private void sendRequestTransaksi(RideCarRequestJson param, final List<DriverModel> driverList) {
        binding.rlprogress.setVisibility(View.VISIBLE);
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        final BookService service = ServiceGenerator.createService(BookService.class, loginUser.getEmail(), loginUser.getPassword());

        service.requestTransaksi(param).enqueue(new Callback<RideCarResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<RideCarResponseJson> call, @NonNull Response<RideCarResponseJson> response) {
                if (response.isSuccessful()) {
                    buildDriverRequest(Objects.requireNonNull(response.body()));
                    thread = new Thread(() -> {
                        for (int i = 0; i < driverList.size(); i++) {
                            fcmBroadcast(i, driverList);
                        }

                        try {
                            Thread.sleep(30000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (threadRun) {
                            CheckStatusTransRequest param1 = new CheckStatusTransRequest();
                            param1.setIdTransaksi(transaksi.getId());
                            service.checkStatusTransaksi(param1).enqueue(new Callback<CheckStatusTransResponse>() {
                                @Override
                                public void onResponse(@NonNull Call<CheckStatusTransResponse> call1, @NonNull Response<CheckStatusTransResponse> response1) {
                                    if (response1.isSuccessful()) {
                                        CheckStatusTransResponse checkStatus = response1.body();
                                        if (!Objects.requireNonNull(checkStatus).isStatus()) {
                                            notif("Pengemudi tidak ditemukan!");
                                            runOnUiThread(() -> notif("Pengemudi tidak ditemukan!"));

                                            new Handler().postDelayed(RentCarActivity.this::finish, 3000);
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(@NonNull Call<CheckStatusTransResponse> call1, @NonNull Throwable t) {
                                    notif("Pengemudi tidak ditemukan!");
                                    runOnUiThread(() -> notif("Pengemudi tidak ditemukan!"));

                                    new Handler().postDelayed(RentCarActivity.this::finish, 3000);

                                }
                            });
                        }

                    });
                    thread.start();


                }
            }

            @Override
            public void onFailure(@NonNull Call<RideCarResponseJson> call, @NonNull Throwable t) {
                t.printStackTrace();
                notif("Akun Anda bermasalah, silakan hubungi layanan pelanggan!");
                new Handler().postDelayed(() -> finish(), 3000);
            }
        });
    }

    private void buildDriverRequest(RideCarResponseJson response) {
        transaksi = response.getData().get(0);
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        if (request == null) {
            request = new DriverRequest();
            request.setIdTransaksi(transaksi.getId());
            request.setIdPelanggan(transaksi.getIdPelanggan());
            request.setRegIdPelanggan(loginUser.getToken());
            request.setOrderFitur(designedFitur.getHome());
            request.setStartLatitude(transaksi.getStartLatitude());
            request.setStartLongitude(transaksi.getStartLongitude());
            request.setEndLatitude(transaksi.getEndLatitude());
            request.setEndLongitude(transaksi.getEndLongitude());
            request.setJarak(transaksi.getJarak());
            request.setHarga(Long.parseLong(totalbiaya + transaksi.getAdminFee()));
            request.setAdminFee(transaksi.getAdminFee());
            request.setWaktuOrder(transaksi.getWaktuOrder());
            request.setAlamatAsal(transaksi.getAlamatAsal());
            request.setAlamatTujuan(transaksi.getAlamatTujuan());
            request.setKodePromo(transaksi.getKodePromo());
            request.setKreditPromo(transaksi.getKreditPromo());
            request.setPakaiWallet(String.valueOf(transaksi.isPakaiWallet()));
            request.setEstimasi(transaksi.getEstimasi());
            request.setLayanan(binding.layanan.getText().toString());
            request.setLayanandesc(binding.layanandes.getText().toString());
            request.setIcon(ICONFITUR);
            request.setBiaya(binding.cost.getText().toString());


            String namaLengkap = String.format("%s", loginUser.getFullnama());
            request.setNamaPelanggan(namaLengkap);
            request.setTelepon(loginUser.getNoTelepon());
            request.setType(ORDER);
        }
    }

    private void fcmBroadcast(int index, List<DriverModel> driverList) {
        DriverModel driverToSend = driverList.get(index);
        request.setTime_accept(new Date().getTime() + "");


        User loginUser = BaseApp.getInstance(this).getLoginUser();
        final NotificationService service = ServiceGenerator.createService(NotificationService.class, loginUser.getEmail(), loginUser.getPassword());

        RequestDriverJson.Data data = new RequestDriverJson.Data(
                request.getIdTransaksi(),
                request.getIdPelanggan(),
                request.getRegIdPelanggan(),
                request.getOrderFitur(),
                request.getHarga(),
                request.getWaktuOrder(),
                request.getAlamatAsal(),
                request.getAlamatTujuan(),
                request.isPakaiWallet(),
                request.getIcon(),
                request.getLayanan(),
                request.getLayanandesc(),
                request.getEstimasi(),
                request.getBiaya(),
                request.getAdminFee(),
                request.getDistance(),
                request.getTokenmerchant(),
                request.getIdtransmerchant(),
                request.getType()
        );

        RequestDriverJson requestDriverJson = new RequestDriverJson(driverToSend.getRegId(), data);

        service.sendNotification(requestDriverJson).enqueue(new Callback<DefaultResponseJson>() {
            @Override
            public void onResponse(Call<DefaultResponseJson> call, Response<DefaultResponseJson> response) {
                Log.d("response_23", response.message());
            }

            @Override
            public void onFailure(Call<DefaultResponseJson> call, Throwable t) {
                Log.d("response_23", t.getMessage());
            }
        });
    }

    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final DriverResponse response) {
        if (response.getResponse().equalsIgnoreCase(DriverResponse.ACCEPT) || response.getResponse().equals("3") || response.getResponse().equals("4")) {
            runOnUiThread(() -> {
                threadRun = false;
                for (DriverModel cDriver : driverAvailable) {
                    if (cDriver.getId().equals(response.getId())) {


                        Intent intent = new Intent(RentCarActivity.this, ProgressActivity.class);
                        intent.putExtra("driver_id", cDriver.getId());
                        intent.putExtra("transaction_id", request.getIdTransaksi());
                        intent.putExtra("response", "2");
                        startActivity(intent);
                        DriverResponse response1 = new DriverResponse();
                        response1.setId("");
                        response1.setIdTransaksi("");
                        response1.setResponse("");
                        EventBus.getDefault().postSticky(response1);
                        finish();
                    }
                }
            });
        }
    }

protected void onStart() {
    super.onStart();
    mapView.onStart();
    EventBus.getDefault().register(this);
}

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();

        User userLogin = BaseApp.getInstance(this).getLoginUser();
        saldoWallet = String.valueOf(userLogin.getWalletSaldo());
        Utility.currencyTXT(binding.balance, saldoWallet, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
