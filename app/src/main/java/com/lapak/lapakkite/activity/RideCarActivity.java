package com.lapak.lapakkite.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.lapak.lapakkite.databinding.ActivityRideBinding;
import com.lapak.lapakkite.json.fcm.DefaultResponseJson;
import com.lapak.lapakkite.json.fcm.RequestDriverJson;
import com.lapak.lapakkite.utils.api.service.NotificationService;
import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.location.LocationEngineResult;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.MapboxDirections;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.core.exceptions.ServicesException;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.lapak.lapakkite.R;
import com.lapak.lapakkite.constants.BaseApp;
import com.lapak.lapakkite.constants.Constant;
import com.lapak.lapakkite.json.CheckStatusTransRequest;
import com.lapak.lapakkite.json.CheckStatusTransResponse;
import com.lapak.lapakkite.json.GetNearRideCarRequestJson;
import com.lapak.lapakkite.json.GetNearRideCarResponseJson;
import com.lapak.lapakkite.json.PromoRequestJson;
import com.lapak.lapakkite.json.PromoResponseJson;
import com.lapak.lapakkite.json.RideCarRequestJson;
import com.lapak.lapakkite.json.RideCarResponseJson;
import com.lapak.lapakkite.json.fcm.DriverRequest;
import com.lapak.lapakkite.json.fcm.DriverResponse;
import com.lapak.lapakkite.models.DriverModel;
import com.lapak.lapakkite.models.ServiceModel;
import com.lapak.lapakkite.models.TransModel;
import com.lapak.lapakkite.models.User;
import com.lapak.lapakkite.utils.Utility;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import com.lapak.lapakkite.utils.api.ServiceGenerator;
import com.lapak.lapakkite.utils.api.service.BookService;
import com.lapak.lapakkite.utils.api.service.UserService;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.mapbox.core.constants.Constants.PRECISION_6;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconRotate;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineCap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility;
import static com.lapak.lapakkite.json.fcm.FCMType.ORDER;

/**
 * Created by Ourdevelops Team on 10/26/2019.
 */

public class RideCarActivity extends AppCompatActivity implements OnMapReadyCallback {
    public static final String FITUR_KEY = "FiturKey";
    String ICONFITUR;
    TransModel transaksi;
    Thread thread;
    boolean threadRun = true;
    private DriverRequest request;
    MapboxMap mapboxMap;
    LocationComponent locationComponent;
    UiSettings uiSettings;

    private ActivityRideBinding binding;
    private List<DriverModel> driverAvailable;
    private ServiceModel designedFitur;
    private double distance;
    private long diskonVoucher, maksimum;
    private String saldoWallet;
    private String checkedpaywallet;
    LatLng pickUpLatLang, destinationLatLang;
    MapView mapView;
    String service, icondriver;

    long minimumKm, adminFee;

    long totalBiaya;
    long subBiaya;
    long biaya;
    long biayaMinimum;

    double diskonDompet;

    Point pickup, destination;

    LocationManager lm;

    ActivityResultLauncher<String> permission;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));
        binding = ActivityRideBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        BottomSheetBehavior behavior = BottomSheetBehavior.from(binding.bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        diskonVoucher = 0;

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        binding.pickUpContainer.setVisibility(View.VISIBLE);
        binding.destinationContainer.setVisibility(View.GONE);
        binding.detail.setVisibility(View.GONE);

        User userLogin = BaseApp.getInstance(this).getLoginUser();
        saldoWallet = String.valueOf(userLogin.getWalletSaldo());

        Realm realm = BaseApp.getInstance(this).getRealmInstance();

        Intent intent = getIntent();
        int fiturId = intent.getIntExtra(FITUR_KEY, -1);
        ICONFITUR = intent.getStringExtra("icon");
        if (fiturId != -1)
            designedFitur = realm.where(ServiceModel.class).equalTo("idFitur", fiturId).findFirst();


        service = String.valueOf(Objects.requireNonNull(designedFitur).getIdFitur());
        biaya = designedFitur.getBiaya();
        biayaMinimum = designedFitur.getBiaya_minimum();
        diskonDompet = designedFitur.getBiayaAkhir();
        minimumKm = designedFitur.getMinimumCostDistance();
        adminFee = designedFitur.getAdminFee();
        icondriver = designedFitur.getIcon_driver();
        maksimum = Long.parseLong(designedFitur.getMaksimumdist());

        binding.ketsaldo.setText("Discount " + designedFitur.getDiskon() + " with Wallet");
        Picasso.get()
                .load(Constant.IMAGESFITUR + ICONFITUR)
                .placeholder(R.drawable.lapak_circle)
                .into(binding.image);

        binding.layanan.setText(designedFitur.getFitur());
        binding.layanandes.setText(designedFitur.getKeterangan());

        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        binding.backBtn.setOnClickListener(view -> finish());

        binding.topUp.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), TopupSaldoActivity.class)));

        binding.btnpromo.setOnClickListener(v -> {
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                Objects.requireNonNull(imm).hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
            } catch (Exception ignored) {

            }
            if (binding.promocode.getText().toString().isEmpty()) {
                notif("Promo code cant be empty!");
            } else {
                promokodedata();
            }
        });

        binding.pickUpText.setOnClickListener(v -> {
            binding.pickUpContainer.setVisibility(View.VISIBLE);
            binding.destinationContainer.setVisibility(View.GONE);
            openAutocompleteActivity(1);
        });

        binding.destinationText.setOnClickListener(v -> {
            binding.destinationContainer.setVisibility(View.VISIBLE);
            binding.pickUpContainer.setVisibility(View.GONE);
            openAutocompleteActivity(2);
        });

        binding.order.setOnClickListener(v -> onOrderButton());


        binding.llcheckedcash.setOnClickListener(view -> {
            totalBiaya = (subBiaya - diskonVoucher) + adminFee;

            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), RideCarActivity.this);
            Utility.currencyTXT(binding.diskon, String.valueOf(diskonVoucher), RideCarActivity.this);

            binding.checkedcash.setSelected(true);
            binding.checkedwallet.setSelected(false);
            checkedpaywallet = "0";
            binding.cashPayment.setTextColor(getResources().getColor(R.color.colorgradient));
            binding.walletpayment.setTextColor(getResources().getColor(R.color.gray));
            binding.checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
            binding.checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
        });

        binding.llcheckedwallet.setOnClickListener(view -> {
            long hasilDiskon = (long) (subBiaya * diskonDompet);
            long totalDiskon = hasilDiskon + diskonVoucher;

            if (Long.parseLong(saldoWallet) >= ((subBiaya - totalDiskon) + adminFee)) {

                totalBiaya = (subBiaya - totalDiskon) + adminFee;

                Utility.currencyTXT(binding.diskon, String.valueOf(totalDiskon), RideCarActivity.this);
                Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), RideCarActivity.this);

                binding.checkedcash.setSelected(false);
                binding.checkedwallet.setSelected(true);
                checkedpaywallet = "1";
                binding.walletpayment.setTextColor(getResources().getColor(R.color.colorgradient));
                binding.cashPayment.setTextColor(getResources().getColor(R.color.gray));
                binding.checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
                binding.checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
            } else {
                notif("Saldo Anda Tidak Cukup");
            }
        });


    }

    @SuppressLint({"MissingPermission", "WrongConstant"})
    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        boolean gps_enabled = Objects.requireNonNull(lm).isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network_enabled = Objects.requireNonNull(lm).isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (PermissionsManager.areLocationPermissionsGranted(this) | gps_enabled && network_enabled) {
            this.mapboxMap = mapboxMap;
            mapboxMap.setStyle(Constant.CUSTOM_MAPBOX_STYLE, style -> {
                uiSettings = mapboxMap.getUiSettings();
                uiSettings.setAttributionEnabled(false);
                uiSettings.setLogoEnabled(false);
                uiSettings.setCompassEnabled(false);
                uiSettings.setRotateGesturesEnabled(false);

                locationComponent = mapboxMap.getLocationComponent();
                locationComponent.activateLocationComponent(
                        LocationComponentActivationOptions
                                .builder(RideCarActivity.this, style)
                                .useDefaultLocationEngine(true)
                                .locationEngineRequest(new LocationEngineRequest.Builder(750)
                                        .setFastestInterval(750)
                                        .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                                        .build())
                                .build());

                locationComponent.setLocationComponentEnabled(true);
                locationComponent.setRenderMode(RenderMode.COMPASS);

                locationComponent.getLocationEngine().getLastLocation(new LocationEngineCallback<LocationEngineResult>() {
                    @Override
                    public void onSuccess(LocationEngineResult result) {
                        if (result.getLastLocation() != null) {
                            CameraPosition position = new CameraPosition.Builder()
                                    .target(new LatLng(result.getLastLocation().getLatitude(), result.getLastLocation().getLongitude()))
                                    .zoom(18)
                                    .tilt(20)
                                    .build();
                            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 100);
                            fetchNearDriver(result.getLastLocation().getLatitude(), result.getLastLocation().getLongitude(), service, style);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Exception exception) {

                    }
                });

                initDroppedMarkerpickup(style);
                initDroppedMarkerdestination(style);
                initDottedLineSourceAndLayer(style);
                binding.currentlocation.setOnClickListener(view -> {
                    locationComponent.getLocationEngine().getLastLocation(new LocationEngineCallback<LocationEngineResult>() {
                        @Override
                        public void onSuccess(LocationEngineResult result) {
                            CameraPosition position = new CameraPosition.Builder()
                                    .target(new LatLng(result.getLastLocation().getLatitude(), result.getLastLocation().getLongitude()))
                                    .zoom(18)
                                    .tilt(20)
                                    .build();
                            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 1000);
                        }

                        @Override
                        public void onFailure(@NonNull Exception exception) {

                        }
                    });
                });
                binding.pickUpButton.setOnClickListener(view -> {
                    LatLng centerPos = mapboxMap.getCameraPosition().target;
                    onPickUp(style, mapboxMap, centerPos);
                });

                binding.destinationButton.setOnClickListener(view -> {
                    LatLng centerPos = mapboxMap.getCameraPosition().target;
                    onDestination(style, mapboxMap, centerPos);
                });
            });
        } else {
            finish();
            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 2);
        }

    }

    private void onPickUp(Style style, MapboxMap mapboxMap, LatLng centerPos) {
        fetchNearDriver(centerPos.getLatitude(), centerPos.getLongitude(), service, style);
        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(centerPos.getLatitude(), centerPos.getLongitude()))
                .zoom(15)
                .tilt(20)
                .build();
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 1000);
        binding.destinationContainer.setVisibility(View.VISIBLE);
        binding.pickUpContainer.setVisibility(View.GONE);
        pickUpLatLang = new LatLng(centerPos.getLatitude(), centerPos.getLongitude());
        if (style.getLayer("DROPPED_MARKER_LAYER_ID_PICKUP") != null) {
            GeoJsonSource source = style.getSourceAs("dropped-marker-source-id-pickup");
            if (source != null) {
                source.setGeoJson(Point.fromLngLat(centerPos.getLongitude(), centerPos.getLatitude()));
            }
            Layer pickUpMarker = style.getLayer("DROPPED_MARKER_LAYER_ID_PICKUP");
            if (pickUpMarker != null) {
                pickUpMarker.setProperties(visibility(Property.VISIBLE));
            }
        }
        binding.textprogress.setVisibility(View.VISIBLE);

        pickup = Point.fromLngLat(centerPos.getLongitude(), centerPos.getLatitude());
        getaddress(pickup, mapboxMap, binding.pickUpText);
    }

    private void onDestination(Style style, MapboxMap mapboxMap, LatLng centerPos) {

        if (style.getLayer("DROPPED_MARKER_LAYER_ID_DEST") != null) {
            GeoJsonSource source = style.getSourceAs("dropped-marker-source-id-dest");
            if (source != null) {
                source.setGeoJson(Point.fromLngLat(centerPos.getLongitude(), centerPos.getLatitude()));
            }
            Layer destMarker = style.getLayer("DROPPED_MARKER_LAYER_ID_DEST");
            if (destMarker != null) {
                destMarker.setProperties(visibility(Property.VISIBLE));
            }
        }
        destinationLatLang = new LatLng(centerPos.getLatitude(), centerPos.getLongitude());
        binding.destinationContainer.setVisibility(View.GONE);
        if (binding.pickUpText.getText().toString().isEmpty()) {
            binding.pickUpContainer.setVisibility(View.VISIBLE);
        } else {
            binding.pickUpContainer.setVisibility(View.GONE);
        }
        destination = Point.fromLngLat(centerPos.getLongitude(), centerPos.getLatitude());
        getaddress(destination, mapboxMap, binding.destinationText);
    }

    private void initDroppedMarkerdestination(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("dropped-icon-image-dest", BitmapFactory.decodeResource(
                getResources(), R.drawable.ic_destination_map));
        loadedMapStyle.addSource(new GeoJsonSource("dropped-marker-source-id-dest"));
        loadedMapStyle.addLayer(new SymbolLayer("DROPPED_MARKER_LAYER_ID_DEST",
                "dropped-marker-source-id-dest").withProperties(
                iconImage("dropped-icon-image-dest"),
                iconAllowOverlap(true),
                visibility(Property.NONE),
                iconSize(2.0f),
                iconIgnorePlacement(true)
        ));

    }

    private void initDroppedMarkerpickup(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("dropped-icon-image-pickup", BitmapFactory.decodeResource(
                getResources(), R.drawable.ic_pikup_map));
        loadedMapStyle.addSource(new GeoJsonSource("dropped-marker-source-id-pickup"));
        loadedMapStyle.addLayer(new SymbolLayer("DROPPED_MARKER_LAYER_ID_PICKUP",
                "dropped-marker-source-id-pickup").withProperties(
                iconImage("dropped-icon-image-pickup"),
                iconAllowOverlap(true),
                visibility(Property.NONE),
                iconSize(2.0f),
                iconIgnorePlacement(true)
        ));

    }

    private void getaddress(final Point point, MapboxMap mapboxMap, TextView textView) {
        try {
            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(getString(R.string.mapbox_access_token))
                    .query(Point.fromLngLat(point.longitude(), point.latitude()))
                    .build();

            client.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(@NonNull Call<GeocodingResponse> call, @NonNull Response<GeocodingResponse> response) {

                    if (response.body() != null) {
                        List<CarmenFeature> results = response.body().features();
                        if (results.size() > 0) {
                            CarmenFeature feature = results.get(0);
                            mapboxMap.getStyle(style -> {
                                textView.setText(feature.placeName());
                                getRoute(mapboxMap);
                            });

                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GeocodingResponse> call, @NonNull Throwable throwable) {
                }
            });
        } catch (ServicesException servicesException) {
            Timber.e("Error geocoding: %s", servicesException.toString());
            servicesException.printStackTrace();
        }
    }

    private void getRoute(MapboxMap mapboxMap) {
        if (pickup != null && destination != null) {
            binding.rlprogress.setVisibility(View.VISIBLE);
            MapboxDirections client = MapboxDirections.builder()
                    .origin(pickup)
                    .destination(destination)
                    .overview(DirectionsCriteria.OVERVIEW_FULL)
                    .profile(DirectionsCriteria.PROFILE_DRIVING_TRAFFIC)
                    .accessToken(getString(R.string.mapbox_access_token))
                    .build();
            client.enqueueCall(new Callback<DirectionsResponse>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<DirectionsResponse> call, @NonNull Response<DirectionsResponse> response) {
                    if (response.body() == null) {
                        Timber.d("No routes found, make sure you set the right user and access token.");
                        return;
                    } else if (response.body().routes().size() < 1) {
                        Timber.d("No routes found");
                        return;
                    }
                    DirectionsRoute currentroute = response.body().routes().get(0);
                    drawNavigationPolylineRoute(currentroute, mapboxMap);
                    binding.rlprogress.setVisibility(View.GONE);
                    binding.destinationContainer.setVisibility(View.GONE);
                    binding.pickUpContainer.setVisibility(View.GONE);
                    String format = String.format(Locale.US, "%.0f", currentroute.distance() / 1000f);
                    long dist = Long.parseLong(format);
                    if (dist < maksimum) {
                        binding.rlprogress.setVisibility(View.GONE);
                        diskonVoucher = 0;
                        binding.promocode.setText("");
                        updateDistance(currentroute.distance());
                        long minutes = (long) ((currentroute.duration() / 60));
                        binding.service.setText(minutes + " mins");
                        String diskontotal = String.valueOf(diskonVoucher);
                        Utility.currencyTXT(binding.diskon, diskontotal, RideCarActivity.this);
                    } else {
                        binding.detail.setVisibility(View.GONE);
                        binding.destinationContainer.setVisibility(View.VISIBLE);
                        binding.rlprogress.setVisibility(View.GONE);
                        notif("destination too far away!");
                    }

                }

                @Override
                public void onFailure(@NonNull Call<DirectionsResponse> call, @NonNull Throwable throwable) {
                    Timber.d("Error: %s", throwable.getMessage());

                }
            });
        }
    }

    private void drawNavigationPolylineRoute(final DirectionsRoute route, MapboxMap mapboxMap) {
        if (mapboxMap != null) {
            mapboxMap.getStyle(style -> {
                List<Feature> directionsRouteFeatureList = new ArrayList<>();
                LineString lineString = LineString.fromPolyline(Objects.requireNonNull(route.geometry()), PRECISION_6);
                List<Point> coordinates = lineString.coordinates();
                for (int i = 0; i < coordinates.size(); i++) {
                    directionsRouteFeatureList.add(Feature.fromGeometry(LineString.fromLngLats(coordinates)));
                }
                FeatureCollection dashedLineDirectionsFeatureCollection = FeatureCollection.fromFeatures(directionsRouteFeatureList);
                GeoJsonSource source = style.getSourceAs("SOURCE_ID");
                if (source != null) {
                    source.setGeoJson(dashedLineDirectionsFeatureCollection);
                }

            });
        }
    }

    private void initDottedLineSourceAndLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addSource(new GeoJsonSource("SOURCE_ID"));
        loadedMapStyle.addLayerBelow(
                new LineLayer(
                        "DIRECTIONS_LAYER_ID", "SOURCE_ID").withProperties(
                        lineCap(Property.LINE_CAP_ROUND),
                        lineJoin(Property.LINE_JOIN_ROUND),
                        lineWidth(6f),
                        lineColor(getResources().getColor(R.color.colorPrimary))
                ), "LAYER_BELOW_ID");
    }

    private void updateDistance(Double distance) {
        BottomSheetBehavior behavior = BottomSheetBehavior.from(binding.bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        binding.detail.setVisibility(View.VISIBLE);
        binding.destinationContainer.setVisibility(View.GONE);
        binding.pickUpContainer.setVisibility(View.GONE);
        binding.order.setVisibility(View.VISIBLE);

        checkedpaywallet = "0";
        binding.checkedcash.setSelected(true);
        binding.checkedwallet.setSelected(false);
        binding.cashPayment.setTextColor(getResources().getColor(R.color.colorgradient));
        binding.walletpayment.setTextColor(getResources().getColor(R.color.gray));
        binding.checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
        binding.checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
        float km = (float) ((distance) / 1000f);

        this.distance = km;

        String format = String.format(Locale.US, "%.1f", km);
        binding.distance.setText(format);

        if (km > minimumKm) {
            long biayaPerKm = (long) (biaya * (km-minimumKm));
            subBiaya = (long) (biayaMinimum + (Math.ceil(biayaPerKm / 500.0) * 500.0));
        } else {
            subBiaya = biayaMinimum;
        }
        totalBiaya = subBiaya + adminFee;
        Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), this);
        Utility.currencyTXT(binding.fee, String.valueOf(adminFee), this);
        Utility.currencyTXT(binding.cost, String.valueOf(subBiaya), this);
    }

    private void openAutocompleteActivity(int request_code) {
        Intent intent = new PlaceAutocomplete.IntentBuilder()
                .accessToken(Mapbox.getAccessToken() != null ? Mapbox.getAccessToken() : getString(R.string.mapbox_access_token))
                .placeOptions(PlaceOptions.builder()
                        .backgroundColor(Color.parseColor("#EEEEEE"))
                        .geocodingTypes("poi", "neighborhood", "locality")
                        .country(new Locale("in", "ID"))
                        .limit(10)
                        .build(PlaceOptions.MODE_CARDS))
                .build(RideCarActivity.this);
        startActivityForResult(intent, request_code);
    }

    public void notif(String text) {
        binding.rlnotif.setVisibility(View.VISIBLE);
        binding.textnotif.setText(text);

        new Handler().postDelayed(() -> binding.rlnotif.setVisibility(View.GONE), 3000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);
                if (mapboxMap != null) {
                    binding.pickUpText.setText(selectedCarmenFeature.placeName());
                    Style style = mapboxMap.getStyle();
                    if (style != null) {
                        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                                new CameraPosition.Builder()
                                        .target(new LatLng(((Point) Objects.requireNonNull(selectedCarmenFeature.geometry())).latitude(),
                                                ((Point) selectedCarmenFeature.geometry()).longitude()))
                                        .zoom(15)
                                        .build()), 4);
                        LatLng centerPos = new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                ((Point) selectedCarmenFeature.geometry()).longitude());
                        onPickUp(style, mapboxMap, centerPos);
                    }
                }
            }
        }
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);
                if (mapboxMap != null) {
                    binding.destinationText.setText(selectedCarmenFeature.placeName());
                    Style style = mapboxMap.getStyle();
                    if (style != null) {
                        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                                new CameraPosition.Builder()
                                        .target(new LatLng(((Point) Objects.requireNonNull(selectedCarmenFeature.geometry())).latitude(),
                                                ((Point) selectedCarmenFeature.geometry()).longitude()))
                                        .zoom(15)
                                        .build()), 4);
                        LatLng centerPos = new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                ((Point) selectedCarmenFeature.geometry()).longitude());
                        onDestination(style, mapboxMap, centerPos);
                    }
                }
            }
        }
    }

    protected void onStart() {
        super.onStart();
        mapView.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();

        User userLogin = BaseApp.getInstance(this).getLoginUser();
        saldoWallet = String.valueOf(userLogin.getWalletSaldo());
        Utility.currencyTXT(binding.balance, saldoWallet, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void fetchNearDriver(double latitude, double longitude, String service, Style style) {
        if (driverAvailable != null) {
            driverAvailable.clear();
            Log.d("2504", String.valueOf(driverAvailable.size()));
        }

        if (mapboxMap != null) {
            User loginUser = BaseApp.getInstance(this).getLoginUser();

            BookService services = ServiceGenerator.createService(BookService.class, loginUser.getEmail(), loginUser.getPassword());
            GetNearRideCarRequestJson param = new GetNearRideCarRequestJson();
            param.setLatitude(latitude);
            param.setLongitude(longitude);
            param.setFitur(service);

            services.getNearRide(param).enqueue(new Callback<GetNearRideCarResponseJson>() {
                @Override
                public void onResponse(@NonNull Call<GetNearRideCarResponseJson> call, @NonNull Response<GetNearRideCarResponseJson> response) {
                    if (response.isSuccessful()) {
                        driverAvailable = Objects.requireNonNull(response.body()).getData();
                        createMarker(style);
                    }
                }

                @Override
                public void onFailure(@NonNull retrofit2.Call<GetNearRideCarResponseJson> call, @NonNull Throwable t) {

                }
            });
        }
    }

    List<Feature> symbolLayerIconFeatureList;

    private void createMarker(Style style) {
        Layer markermap;
        switch (icondriver) {
            case "1":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    markerRide(style, R.drawable.drivermap, driver.getId() + "f", driver.getId() + "id", driver.getId() + "l");
                    markermap = style.getLayer(driver.getId() + "l");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs(driver.getId() + "id");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));

                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));


                    }
                }
                break;
            case "2":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    markerRide(style, R.drawable.carmap, driver.getId() + "f", driver.getId() + "id", driver.getId() + "l");
                    markermap = style.getLayer(driver.getId() + "l");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs(driver.getId() + "id");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }

                }
                break;
            case "3":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    markerRide(style, R.drawable.truck, driver.getId() + "f", driver.getId() + "id", driver.getId() + "l");
                    markermap = style.getLayer(driver.getId() + "l");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs(driver.getId() + "id");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }

                }
                break;
            case "4":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    markerRide(style, R.drawable.delivery, driver.getId() + "f", driver.getId() + "id", driver.getId() + "l");
                    markermap = style.getLayer(driver.getId() + "l");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs(driver.getId() + "id");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }

                }
                break;
            case "5":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    markerRide(style, R.drawable.hatchback, driver.getId() + "f", driver.getId() + "id", driver.getId() + "l");
                    markermap = style.getLayer(driver.getId() + "l");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs(driver.getId() + "id");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }

                }
                break;
            case "6":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    markerRide(style, R.drawable.suv, driver.getId() + "f", driver.getId() + "id", driver.getId() + "l");
                    markermap = style.getLayer(driver.getId() + "l");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs(driver.getId() + "id");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }

                }
                break;
            case "7":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    markerRide(style, R.drawable.van, driver.getId() + "f", driver.getId() + "id", driver.getId() + "l");
                    markermap = style.getLayer(driver.getId() + "l");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs(driver.getId() + "id");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }

                }
                break;
            case "8":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    markerRide(style, R.drawable.bicycle, driver.getId() + "f", driver.getId() + "id", driver.getId() + "l");
                    markermap = style.getLayer(driver.getId() + "l");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs(driver.getId() + "id");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }

                }
                break;
            case "9":
                symbolLayerIconFeatureList = new ArrayList<>();
                for (DriverModel driver : driverAvailable) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(driver.getLongitude(), driver.getLatitude())));
                    markerRide(style, R.drawable.bajaj, driver.getId() + "f", driver.getId() + "id", driver.getId() + "l");
                    markermap = style.getLayer(driver.getId() + "l");
                    if (markermap != null) {
                        GeoJsonSource source = style.getSourceAs(driver.getId() + "id");

                        if (source != null) {
                            source.setGeoJson(Point.fromLngLat(driver.getLongitude(), driver.getLatitude()));
                        }
                        markermap.setProperties(visibility(Property.VISIBLE));
                        markermap.setProperties(iconRotate(Float.valueOf(driver.getBearing())));
                    }

                }
                break;
        }
    }

    private void markerRide(@NonNull Style loadedMapStyle, int drawable, String imageid, String driverid, String layerid) {
        try {
            loadedMapStyle.removeLayer(layerid);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        loadedMapStyle.removeSource(driverid);
        loadedMapStyle.addImage(imageid, BitmapFactory.decodeResource(
                getResources(), drawable));

        loadedMapStyle.addSource(new GeoJsonSource(driverid));
        loadedMapStyle.addLayer(new SymbolLayer(layerid,
                driverid).withProperties(
                iconImage(imageid),
                iconAllowOverlap(true),
                visibility(Property.NONE),
                iconSize(1.0f),
                iconIgnorePlacement(true)
        ));


    }

    @SuppressLint("SetTextI18n")
    private void promokodedata() {
        binding.btnpromo.setEnabled(false);
        binding.btnpromo.setText("Wait...");
        final User user = BaseApp.getInstance(this).getLoginUser();
        PromoRequestJson request = new PromoRequestJson();
        request.setFitur(service);
        request.setCode(binding.promocode.getText().toString());

        UserService service = ServiceGenerator.createService(UserService.class, user.getNoTelepon(), user.getPassword());
        service.promocode(request).enqueue(new Callback<PromoResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<PromoResponseJson> call, @NonNull Response<PromoResponseJson> response) {
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("success")) {
                        binding.btnpromo.setEnabled(true);
                        binding.btnpromo.setText("Use");
                        if (response.body().getType().equals("persen")) {
                            diskonVoucher = (Long.parseLong(response.body().getNominal()) * subBiaya) / 100;
                        } else {
                            diskonVoucher = Long.parseLong(response.body().getNominal());
                        }
                        if (checkedpaywallet.equals("1")) {
                            long hasilDiskonDompet = (long) (subBiaya * diskonDompet);
                            long totalDiskon = hasilDiskonDompet + diskonVoucher;
                            totalBiaya = (subBiaya - totalDiskon) + adminFee;
                            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), RideCarActivity.this);
                            Utility.currencyTXT(binding.diskon, String.valueOf(totalDiskon), RideCarActivity.this);
                        } else {
                            totalBiaya = (subBiaya - diskonVoucher) + adminFee;
                            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), RideCarActivity.this);
                            Utility.currencyTXT(binding.diskon, String.valueOf(diskonVoucher), RideCarActivity.this);
                        }
                    } else {
                        binding.btnpromo.setEnabled(true);
                        binding.btnpromo.setText("Use");
                        notif("promo code not available!");
                        diskonVoucher = 0;
                        if (checkedpaywallet.equals("1")) {
                            long hasilDiskonDompet = (long) (subBiaya * diskonDompet);
                            long totalDiskon = hasilDiskonDompet + diskonVoucher;
                            totalBiaya = (subBiaya - totalDiskon) + adminFee;
                            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), RideCarActivity.this);
                            Utility.currencyTXT(binding.diskon, String.valueOf(totalDiskon), RideCarActivity.this);
                        } else {
                            totalBiaya = (subBiaya - diskonVoucher) + adminFee;
                            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), RideCarActivity.this);
                            Utility.currencyTXT(binding.diskon, String.valueOf(diskonVoucher), RideCarActivity.this);
                        }
                    }
                } else {
                    notif("error!");
                }
            }

            @Override
            public void onFailure(@NonNull Call<PromoResponseJson> call, @NonNull Throwable t) {
                t.printStackTrace();
                notif("error");
            }
        });
    }

    //
//
    private void onOrderButton() {
        if (checkedpaywallet.equals("1")) {
            if (driverAvailable.isEmpty()) {
                notif("Maaf, tidak ada pengemudi di sekitar Anda.");
            } else {
                RideCarRequestJson param = new RideCarRequestJson();
                User userLogin = BaseApp.getInstance(this).getLoginUser();
                param.setIdPelanggan(userLogin.getId());
                param.setOrderFitur(service);
                param.setStartLatitude(pickUpLatLang.getLatitude());
                param.setStartLongitude(pickUpLatLang.getLongitude());
                param.setEndLatitude(destinationLatLang.getLatitude());
                param.setEndLongitude(destinationLatLang.getLongitude());
                param.setJarak(this.distance);
                param.setHarga(subBiaya);
                param.setAdminFee(adminFee);
                param.setEstimasi(binding.service.getText().toString());
                param.setKreditpromo(String.valueOf((diskonDompet * subBiaya) + diskonVoucher));
                param.setAlamatAsal(binding.pickUpText.getText().toString());
                param.setAlamatTujuan(binding.destinationText.getText().toString());
                param.setPakaiWallet(1);
                sendRequestTransaksi(param, driverAvailable);
            }
        } else {
            if (driverAvailable.isEmpty()) {
                notif("Maaf, tidak ada pengemudi di sekitar Anda.");
            } else {
                RideCarRequestJson param = new RideCarRequestJson();
                User userLogin = BaseApp.getInstance(this).getLoginUser();
                param.setIdPelanggan(userLogin.getId());
                param.setOrderFitur(service);
                param.setStartLatitude(pickUpLatLang.getLatitude());
                param.setStartLongitude(pickUpLatLang.getLongitude());
                param.setEndLatitude(destinationLatLang.getLatitude());
                param.setEndLongitude(destinationLatLang.getLongitude());
                param.setJarak(this.distance);
                param.setAdminFee(adminFee);
                param.setHarga(subBiaya);
                param.setEstimasi(binding.service.getText().toString());
                param.setKreditpromo(String.valueOf(diskonVoucher));
                param.setAlamatAsal(binding.pickUpText.getText().toString());
                param.setAlamatTujuan(binding.destinationText.getText().toString());
                param.setPakaiWallet(0);

                sendRequestTransaksi(param, driverAvailable);
            }
        }
    }

    private void sendRequestTransaksi(RideCarRequestJson param, final List<DriverModel> driverList) {
        binding.rlprogress.setVisibility(View.VISIBLE);
        binding.textprogress.setText(getString(R.string.waiting_desc));
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        final BookService service = ServiceGenerator.createService(BookService.class, loginUser.getEmail(), loginUser.getPassword());

        service.requestTransaksi(param).enqueue(new Callback<RideCarResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<RideCarResponseJson> call, @NonNull Response<RideCarResponseJson> response) {
                if (response.isSuccessful()) {
                    buildDriverRequest(Objects.requireNonNull(response.body()));
                    thread = new Thread(() -> {
                        for (int i = 0; i < driverList.size(); i++) {
                            fcmBroadcast(i, driverList);
                        }

                        try {
                            Thread.sleep(30000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (threadRun) {
                            CheckStatusTransRequest param1 = new CheckStatusTransRequest();
                            param1.setIdTransaksi(transaksi.getId());
                            service.checkStatusTransaksi(param1).enqueue(new Callback<CheckStatusTransResponse>() {
                                @Override
                                public void onResponse(@NonNull Call<CheckStatusTransResponse> call1, @NonNull Response<CheckStatusTransResponse> response1) {
                                    if (response1.isSuccessful()) {
                                        CheckStatusTransResponse checkStatus = response1.body();
                                        if (!Objects.requireNonNull(checkStatus).isStatus()) {
                                            notif("Pengemudi tidak ditemukan!");
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    notif("Pengemudi tidak ditemukan!");
                                                }
                                            });

                                            new Handler().postDelayed(RideCarActivity.this::finish, 3000);
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(@NonNull Call<CheckStatusTransResponse> call1, @NonNull Throwable t) {
                                    notif("Pengemudi tidak ditemukan!");
                                    runOnUiThread(() -> notif("Pengemudi tidak ditemukan!"));

                                    new Handler().postDelayed(RideCarActivity.this::finish, 3000);

                                }
                            });
                        }

                    });
                    thread.start();


                }
            }

            @Override
            public void onFailure(@NonNull Call<RideCarResponseJson> call, @NonNull Throwable t) {
                t.printStackTrace();
                notif("Akun Anda bermasalah, silakan hubungi layanan pelanggan!");
                new Handler().postDelayed(() -> finish(), 3000);
            }
        });
    }

    private void buildDriverRequest(RideCarResponseJson response) {
        transaksi = response.getData().get(0);
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        if (request == null) {
            request = new DriverRequest();
            request.setIdTransaksi(transaksi.getId());
            request.setIdPelanggan(transaksi.getIdPelanggan());
            request.setRegIdPelanggan(loginUser.getToken());
            request.setOrderFitur(designedFitur.getHome());
            request.setStartLatitude(transaksi.getStartLatitude());
            request.setStartLongitude(transaksi.getStartLongitude());
            request.setEndLatitude(transaksi.getEndLatitude());
            request.setEndLongitude(transaksi.getEndLongitude());
            request.setJarak(transaksi.getJarak());
            request.setHarga(totalBiaya);
            request.setAdminFee(transaksi.getAdminFee());
            request.setWaktuOrder(transaksi.getWaktuOrder());
            request.setAlamatAsal(transaksi.getAlamatAsal());
            request.setAlamatTujuan(transaksi.getAlamatTujuan());
            request.setKodePromo(transaksi.getKodePromo());
            request.setKreditPromo(transaksi.getKreditPromo());
            request.setPakaiWallet(String.valueOf(transaksi.isPakaiWallet()));
            request.setEstimasi(transaksi.getEstimasi());
            request.setLayanan(binding.layanan.getText().toString());
            request.setLayanandesc(binding.layanandes.getText().toString());
            request.setIcon(ICONFITUR);
            request.setBiaya(binding.cost.getText().toString());
            request.setDistance(binding.distance.getText().toString());


            String namaLengkap = String.format("%s", loginUser.getFullnama());
            request.setNamaPelanggan(namaLengkap);
            request.setTelepon(loginUser.getNoTelepon());
            request.setType(ORDER);
        }
    }

    private void fcmBroadcast(int index, List<DriverModel> driverList) {
        DriverModel driverToSend = driverList.get(index);
        request.setTime_accept(new Date().getTime() + "");

        User loginUser = BaseApp.getInstance(this).getLoginUser();
        final NotificationService service = ServiceGenerator.createService(NotificationService.class, loginUser.getEmail(), loginUser.getPassword());

        RequestDriverJson.Data data = new RequestDriverJson.Data(
                request.getIdTransaksi(),
                request.getIdPelanggan(),
                request.getRegIdPelanggan(),
                request.getOrderFitur(),
                request.getHarga(),
                request.getWaktuOrder(),
                request.getAlamatAsal(),
                request.getAlamatTujuan(),
                request.isPakaiWallet(),
                request.getIcon(),
                request.getLayanan(),
                request.getLayanandesc(),
                request.getEstimasi(),
                request.getBiaya(),
                request.getAdminFee(),
                request.getDistance(),
                request.getTokenmerchant(),
                request.getIdtransmerchant(),
                request.getType()
        );

        RequestDriverJson requestDriverJson = new RequestDriverJson(driverToSend.getRegId(), data);

        service.sendNotification(requestDriverJson).enqueue(new Callback<DefaultResponseJson>() {
            @Override
            public void onResponse(Call<DefaultResponseJson> call, Response<DefaultResponseJson> response) {
                com.lapak.lapakkite.utils.Log.d("response_23", response.message());
            }

            @Override
            public void onFailure(Call<DefaultResponseJson> call, Throwable t) {
                com.lapak.lapakkite.utils.Log.d("response_23", t.getMessage());
            }
        });
    }

    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final DriverResponse response) {
        Log.e("DRIVER RESPONSE (W)", response.getResponse() + " " + response.getId() + " " + response.getIdTransaksi());
        if (response.getResponse().equalsIgnoreCase(DriverResponse.ACCEPT) || response.getResponse().equals("3") || response.getResponse().equals("4")) {
            runOnUiThread(new Runnable() {
                public void run() {
                    threadRun = false;
                    for (DriverModel cDriver : driverAvailable) {
                        if (cDriver.getId().equals(response.getId())) {
                            Intent intent = new Intent(RideCarActivity.this, ProgressActivity.class);
                            intent.putExtra("driver_id", cDriver.getId());
                            intent.putExtra("transaction_id", request.getIdTransaksi());
                            intent.putExtra("response", "2");
                            startActivity(intent);
                            DriverResponse response = new DriverResponse();
                            response.setId("");
                            response.setIdTransaksi("");
                            response.setResponse("");
                            EventBus.getDefault().postSticky(response);
                            finish();
                        }
                    }
                }
            });
        }
    }

    private void GetCurrentlocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            getLocationPermission();
            return;
        }


    }

    private void getLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    123);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                GetCurrentlocation();
            } else {
                Toast.makeText(this, "Please Grant permission", Toast.LENGTH_SHORT).show();
            }
        }

    }

}
