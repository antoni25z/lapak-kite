package com.lapak.lapakkite.activity;

import static com.lapak.lapakkite.activity.SendActivity.FITUR_KEY;
import static com.lapak.lapakkite.json.fcm.FCMType.ORDER;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.lapak.lapakkite.R;
import com.lapak.lapakkite.constants.BaseApp;
import com.lapak.lapakkite.databinding.ActivitySendDetailBinding;
import com.lapak.lapakkite.json.CheckStatusTransRequest;
import com.lapak.lapakkite.json.CheckStatusTransResponse;
import com.lapak.lapakkite.json.PromoRequestJson;
import com.lapak.lapakkite.json.PromoResponseJson;
import com.lapak.lapakkite.json.SendRequestJson;
import com.lapak.lapakkite.json.SendResponseJson;
import com.lapak.lapakkite.json.fcm.DefaultResponseJson;
import com.lapak.lapakkite.json.fcm.DriverRequest;
import com.lapak.lapakkite.json.fcm.DriverResponse;
import com.lapak.lapakkite.json.fcm.RequestDriverJson;
import com.lapak.lapakkite.models.DriverModel;
import com.lapak.lapakkite.models.ServiceModel;
import com.lapak.lapakkite.models.TransSendModel;
import com.lapak.lapakkite.models.User;
import com.lapak.lapakkite.utils.Log;
import com.lapak.lapakkite.utils.Utility;
import com.lapak.lapakkite.utils.api.ServiceGenerator;
import com.lapak.lapakkite.utils.api.service.BookService;
import com.lapak.lapakkite.utils.api.service.NotificationService;
import com.lapak.lapakkite.utils.api.service.UserService;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.yesterselga.countrypicker.CountryPicker;
import com.yesterselga.countrypicker.CountryPickerListener;
import com.yesterselga.countrypicker.Theme;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ourdevelops Team on 10/26/2019.
 */

public class SendDetailActivity extends AppCompatActivity {

    private ActivitySendDetailBinding binding;
    String itemdetail, service;
    String country_iso_code = "en";

    private double distance;
    private LatLng pickUpLatLang;
    private LatLng destinationLatLang;
    private String pickup, icon, layanan, layanandesk;
    private String destination;
    private String biayaakhir;
    private ArrayList<DriverModel> driverAvailable;
    private ServiceModel fiturModel;
    Realm realm;
    private String saldoWallet;
    private String checkedpaywallet;
    private long diskonVoucher;
    TransSendModel transaksi;
    private DriverRequest request;
    Thread thread;
    boolean threadRun = true;

    long minimumKm, adminFee;

    long totalBiaya;
    long subBiaya;
    long biaya;
    long biayaMinimum;

    double diskonDompet;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySendDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        diskonVoucher = 0;
        
        realm = BaseApp.getInstance(this).getRealmInstance();
        
        User userLogin = BaseApp.getInstance(this).getLoginUser();
        saldoWallet = String.valueOf(userLogin.getWalletSaldo());
        
        Intent intent = getIntent();
        distance = intent.getDoubleExtra("distance", 0);
        pickUpLatLang = intent.getParcelableExtra("pickup_latlng");
        destinationLatLang = intent.getParcelableExtra("destination_latlng");
        pickup = intent.getStringExtra("pickup");
        icon = intent.getStringExtra("icon");
        layanan = intent.getStringExtra("layanan");
        layanandesk = intent.getStringExtra("layanandesk");
        destination = intent.getStringExtra("destination");
        
        String timeDistance = intent.getStringExtra("time_distance");
        driverAvailable = (ArrayList<DriverModel>) intent.getSerializableExtra("driver");
        int selectedFitur = intent.getIntExtra(FITUR_KEY, -1);

        binding.backBtn.setOnClickListener(view -> finish());

        binding.topUp.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), TopupSaldoActivity.class)));

        if (selectedFitur != -1)
            fiturModel = realm.where(ServiceModel.class).equalTo("idFitur", selectedFitur).findFirst();
        assert fiturModel != null;
        
        service = String.valueOf(fiturModel.getIdFitur());
        biaya = fiturModel.getBiaya();
        biayaMinimum = fiturModel.getBiaya_minimum();
        diskonDompet = fiturModel.getBiayaAkhir();
        minimumKm = fiturModel.getMinimumCostDistance();
        adminFee = fiturModel.getAdminFee();


        binding.service.setText(timeDistance);
        float km = ((float) distance);
        String format = String.format(Locale.US, "%.1f", km);
        binding.distance.setText(format);
        Utility.currencyTXT(binding.cost, String.valueOf(subBiaya), this);
        Utility.currencyTXT(binding.diskon, String.valueOf(diskonVoucher), SendDetailActivity.this);
        binding.ketsaldo.setText("Discount " + fiturModel.getDiskon() + " with Wallet");

        checkedpaywallet = "0";
        binding.checkedcash.setSelected(true);
        binding.checkedwallet.setSelected(false);
        binding.cashPayment.setTextColor(getResources().getColor(R.color.colorgradient));
        binding.walletpayment.setTextColor(getResources().getColor(R.color.gray));
        binding.checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
        binding.checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.gray));

        if (km > minimumKm) {
            long biayaPerKm = (long) (biaya * (km-minimumKm));
            subBiaya = biayaMinimum + biayaPerKm;
        } else {
            subBiaya = biayaMinimum;
        }
        totalBiaya = subBiaya + adminFee;
        Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), this);
        Utility.currencyTXT(binding.fee, String.valueOf(adminFee), this);
        Utility.currencyTXT(binding.cost, String.valueOf(subBiaya), this);

        binding.dokumen.setSelected(true);
        binding.fashion.setSelected(false);
        binding.box.setSelected(false);
        binding.other.setSelected(false);
        itemdetail = "document";
        binding.otherdetail.setVisibility(View.GONE);

        binding.llcheckedcash.setOnClickListener(view -> {
            totalBiaya = (subBiaya - diskonVoucher) + adminFee;

            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), this);
            Utility.currencyTXT(binding.diskon, String.valueOf(diskonVoucher), this);

            binding.checkedcash.setSelected(true);
            binding.checkedwallet.setSelected(false);
            checkedpaywallet = "0";
            binding.cashPayment.setTextColor(getResources().getColor(R.color.colorgradient));
            binding.walletpayment.setTextColor(getResources().getColor(R.color.gray));
            binding.checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
            binding.checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
        });

        binding.llcheckedwallet.setOnClickListener(view -> {
            long hasilDiskon = (long) (subBiaya * diskonDompet);
            long totalDiskon = hasilDiskon + diskonVoucher;

            if (Long.parseLong(saldoWallet) >= ((subBiaya - totalDiskon) + adminFee)) {
                totalBiaya = (subBiaya - totalDiskon) + adminFee;
                Utility.currencyTXT(binding.diskon, String.valueOf(totalDiskon), this);
                Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), this);

                binding.checkedcash.setSelected(false);
                binding.checkedwallet.setSelected(true);
                checkedpaywallet = "1";
                binding.walletpayment.setTextColor(getResources().getColor(R.color.colorgradient));
                binding.cashPayment.setTextColor(getResources().getColor(R.color.gray));
                binding.checkedwallet.setBackgroundTintList(getResources().getColorStateList(R.color.colorgradient));
                binding.checkedcash.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
            } else {
                notif("Saldo Tidak Cukup");
            }
        });

        binding.dokumen.setOnClickListener(view -> {
            binding.dokumen.setSelected(true);
            binding.fashion.setSelected(false);
            binding.box.setSelected(false);
            binding.other.setSelected(false);
            itemdetail = "document";
            binding.otherdetail.setVisibility(View.GONE);
            binding.otherdetail.setText("");
        });

        binding.fashion.setOnClickListener(view -> {
            binding.dokumen.setSelected(false);
            binding.fashion.setSelected(true);
            binding.box.setSelected(false);
            binding.other.setSelected(false);
            itemdetail = "fashion";
            binding.otherdetail.setVisibility(View.GONE);
            binding.otherdetail.setText("");
        });

        binding.box.setOnClickListener(view -> {
            binding.dokumen.setSelected(false);
            binding.fashion.setSelected(false);
            binding.box.setSelected(true);
            binding.other.setSelected(false);
            itemdetail = "box";
            binding.otherdetail.setVisibility(View.GONE);
            binding.otherdetail.setText("");
        });

        binding.other.setOnClickListener(view -> {
            binding.dokumen.setSelected(false);
            binding.fashion.setSelected(false);
            binding.box.setSelected(false);
            binding.other.setSelected(true);
            binding.otherdetail.setVisibility(View.VISIBLE);
        });

        binding.countrycode.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                final CountryPicker picker = CountryPicker.newInstance("Select Country", Theme.DARK);
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                        binding.countrycode.setText(dialCode);
                        picker.dismiss();
                        country_iso_code = code;
                    }
                });
                picker.setStyle(R.style.countrypicker_style, R.style.countrypicker_style);
                picker.show(getSupportFragmentManager(), "Select Country");
            }
        });

        binding.countrycodereceiver.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                final CountryPicker picker = CountryPicker.newInstance ("Select Country", Theme.DARK);
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                        binding.countrycodereceiver.setText(dialCode);
                        picker.dismiss();
                        country_iso_code = code;
                    }
                });
                picker.setStyle(R.style.countrypicker_style, R.style.countrypicker_style);
                picker.show(getSupportFragmentManager(), "Select Country");
            }
        });

        binding.order.setOnClickListener(view -> {
            if (binding.sendername.getText().toString().isEmpty()) {
                notif("Nama Pengirim tidak boleh kosong!");
            } else if (binding.phonenumber.getText().toString().isEmpty()) {
                notif("Telepon pengirim tidak boleh kosong!");
            } else if (binding.recievername.getText().toString().isEmpty()) {
                notif("Nama Penerima tidak boleh kosong!");
            } else if (binding.phonenumberreceiever.getText().toString().isEmpty()) {
                notif("Telepon penerima tidak boleh kosong!");
            } else {
                onOrderButton();
            }
        });

        binding.btnpromo.setOnClickListener(v -> {
            try  {
                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                Objects.requireNonNull(imm).hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
            } catch (Exception ignored) {

            }
            if (binding.promocode.getText().toString().isEmpty()){
                notif("Kode promo tidak boleh kosong!");
            } else {
                promokodedata();
            }
        });


    }

    @SuppressLint("SetTextI18n")
    private void promokodedata() {
        binding.btnpromo.setEnabled(false);
        binding.btnpromo.setText("Wait...");
        final User user = BaseApp.getInstance(this).getLoginUser();
        PromoRequestJson request = new PromoRequestJson();
        request.setFitur(service);
        request.setCode(binding.promocode.getText().toString());

        UserService service = ServiceGenerator.createService(UserService.class, user.getNoTelepon(), user.getPassword());
        service.promocode(request).enqueue(new Callback<PromoResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<PromoResponseJson> call, @NonNull Response<PromoResponseJson> response) {
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("success")) {
                        binding.btnpromo.setEnabled(true);
                        binding.btnpromo.setText("Use");
                        if (response.body().getType().equals("persen")) {
                            diskonVoucher = (Long.parseLong(response.body().getNominal()) * subBiaya) / 100;
                        } else {
                            diskonVoucher = Long.parseLong(response.body().getNominal());
                        }
                        if (checkedpaywallet.equals("1")) {
                            long hasilDiskonDompet = (long) (subBiaya * diskonDompet);
                            long totalDiskon = hasilDiskonDompet + diskonVoucher;
                            totalBiaya = (subBiaya - totalDiskon) + adminFee;
                            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), SendDetailActivity.this);
                            Utility.currencyTXT(binding.diskon, String.valueOf(totalDiskon), SendDetailActivity.this);
                        } else {
                            totalBiaya = (subBiaya - diskonVoucher) + adminFee;
                            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), SendDetailActivity.this);
                            Utility.currencyTXT(binding.diskon, String.valueOf(diskonVoucher), SendDetailActivity.this);
                        }
                    } else {
                        binding.btnpromo.setEnabled(true);
                        binding.btnpromo.setText("Use");
                        notif("Kode promo tidak boleh kosong!");
                        diskonVoucher = 0;
                        if (checkedpaywallet.equals("1")) {
                            long hasilDiskonDompet = (long) (subBiaya * diskonDompet);
                            long totalDiskon = hasilDiskonDompet + diskonVoucher;
                            totalBiaya = (subBiaya - totalDiskon) + adminFee;
                            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), SendDetailActivity.this);
                            Utility.currencyTXT(binding.diskon, String.valueOf(totalDiskon), SendDetailActivity.this);
                        } else {
                            totalBiaya = (subBiaya - diskonVoucher) + adminFee;
                            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), SendDetailActivity.this);
                            Utility.currencyTXT(binding.diskon, String.valueOf(diskonVoucher), SendDetailActivity.this);
                        }
                    }
                } else {
                    notif("error!");
                }
            }

            @Override
            public void onFailure(@NonNull Call<PromoResponseJson> call, @NonNull Throwable t) {
                t.printStackTrace();
                notif("error");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        User userLogin = BaseApp.getInstance(this).getLoginUser();
        saldoWallet = String.valueOf(userLogin.getWalletSaldo());

        Utility.currencyTXT(binding.balance, saldoWallet, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void onOrderButton() {
        if (checkedpaywallet.equals("1")) {
            if (driverAvailable.isEmpty()) {
                notif("Maaf, tidak ada pengemudi di sekitar Anda.");
            } else {
                binding.rlprogress.setVisibility(View.VISIBLE);
                SendRequestJson param = new SendRequestJson();
                User userLogin = BaseApp.getInstance(this).getLoginUser();
                param.setIdPelanggan(userLogin.getId());
                param.setOrderFitur(String.valueOf(fiturModel.getIdFitur()));
                param.setStartLatitude(pickUpLatLang.getLatitude());
                param.setStartLongitude(pickUpLatLang.getLongitude());
                param.setEndLatitude(destinationLatLang.getLatitude());
                param.setEndLongitude(destinationLatLang.getLongitude());
                param.setJarak(distance);
                param.setHarga(subBiaya);
                param.setAdmin_fee(adminFee);
                param.setEstimasi(binding.service.getText().toString());
                param.setKreditpromo(String.valueOf((diskonDompet * biaya) + diskonVoucher));
                param.setAlamatAsal(pickup);
                param.setAlamatTujuan(destination);
                param.setPakaiWallet(1);
                param.setNamaPengirim(binding.sendername.getText().toString());
                param.setTeleponPengirim(binding.countrycode.getText().toString() + binding.phonenumber.getText().toString());
                param.setNamaPenerima(binding.recievername.getText().toString());
                param.setTeleponPenerima(binding.countrycodereceiver.getText().toString() + binding.phonenumberreceiever.getText().toString());
                if (!binding.otherdetail.getText().toString().isEmpty()) {
                    param.setNamaBarang(binding.otherdetail.getText().toString());
                } else {
                    param.setNamaBarang(itemdetail);
                }
                sendRequestTransaksi(param, driverAvailable);
            }
        } else {
            if (driverAvailable.isEmpty()) {
                notif("Maaf, tidak ada pengemudi di sekitar Anda.");
            } else {
                binding.rlprogress.setVisibility(View.VISIBLE);
                SendRequestJson param = new SendRequestJson();
                User userLogin = BaseApp.getInstance(this).getLoginUser();
                param.setIdPelanggan(userLogin.getId());
                param.setOrderFitur(String.valueOf(fiturModel.getIdFitur()));
                param.setStartLatitude(pickUpLatLang.getLatitude());
                param.setStartLongitude(pickUpLatLang.getLongitude());
                param.setEndLatitude(destinationLatLang.getLatitude());
                param.setEndLongitude(destinationLatLang.getLongitude());
                param.setJarak(distance);
                param.setHarga(subBiaya);
                param.setAdmin_fee(adminFee);
                param.setEstimasi(binding.service.getText().toString());
                param.setKreditpromo(String.valueOf(diskonVoucher));
                param.setAlamatAsal(pickup);
                param.setAlamatTujuan(destination);
                param.setPakaiWallet(0);
                param.setNamaPengirim(binding.sendername.getText().toString());
                param.setTeleponPengirim(binding.countrycode.getText().toString() + binding.phonenumber.getText().toString());
                param.setNamaPenerima(binding.recievername.getText().toString());
                param.setTeleponPenerima(binding.countrycodereceiver.getText().toString() + binding.phonenumberreceiever.getText().toString());
                if (!binding.otherdetail.getText().toString().isEmpty()) {
                    param.setNamaBarang(binding.otherdetail.getText().toString());
                } else {
                    param.setNamaBarang(itemdetail);
                }

                sendRequestTransaksi(param, driverAvailable);
            }
        }
    }

    public void notif(String text) {
        binding.rlnotif.setVisibility(View.VISIBLE);
        binding.textnotif.setText(text);

        new Handler().postDelayed(() -> binding.rlnotif.setVisibility(View.GONE), 3000);
    }

    private void sendRequestTransaksi(SendRequestJson param, final List<DriverModel> driverList) {
        binding.rlprogress.setVisibility(View.VISIBLE);
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        final BookService service = ServiceGenerator.createService(BookService.class, loginUser.getEmail(), loginUser.getPassword());

        service.requestTransaksisend(param).enqueue(new Callback<SendResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<SendResponseJson> call, @NonNull Response<SendResponseJson> response) {
                if (response.isSuccessful()) {
                    buildDriverRequest(Objects.requireNonNull(response.body()));
                    thread = new Thread(() -> {
                        for (int i = 0; i < driverList.size(); i++) {
                            fcmBroadcast(i, driverList);
                        }

                        try {
                            Thread.sleep(30000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (threadRun) {
                            CheckStatusTransRequest param1 = new CheckStatusTransRequest();
                            param1.setIdTransaksi(transaksi.getId());
                            service.checkStatusTransaksi(param1).enqueue(new Callback<CheckStatusTransResponse>() {
                                @Override
                                public void onResponse(@NonNull Call<CheckStatusTransResponse> call1, @NonNull Response<CheckStatusTransResponse> response1) {
                                    if (response1.isSuccessful()) {
                                        CheckStatusTransResponse checkStatus = response1.body();
                                        if (!Objects.requireNonNull(checkStatus).isStatus()) {
                                            notif("Pengemudi tidak ditemukan!");
                                            runOnUiThread(() -> notif("Pengemudi tidak ditemukan!"));

                                            binding.rlprogress.setVisibility(View.GONE);
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(@NonNull Call<CheckStatusTransResponse> call1, @NonNull Throwable t) {
                                    notif("Pengemudi tidak ditemukan!");
                                    runOnUiThread(() -> {
                                        notif("Pengemudi tidak ditemukan!");
                                        binding.rlprogress.setVisibility(View.GONE);
                                    });

                                    binding.rlprogress.setVisibility(View.GONE);

                                }
                            });
                        }

                    });
                    thread.start();


                }
            }

            @Override
            public void onFailure(@NonNull Call<SendResponseJson> call, @NonNull Throwable t) {
                t.printStackTrace();
                notif("Akun Anda bermasalah, silakan hubungi layanan pelanggan!");
                binding.rlprogress.setVisibility(View.GONE);
            }
        });
    }

    private void buildDriverRequest(SendResponseJson response) {
        transaksi = response.getData().get(0);
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        if (request == null) {
            request = new DriverRequest();
            request.setIdTransaksi(transaksi.getId());
            request.setIdPelanggan(transaksi.getIdPelanggan());
            request.setRegIdPelanggan(loginUser.getToken());
            request.setOrderFitur(fiturModel.getHome());
            request.setStartLatitude(transaksi.getStartLatitude());
            request.setStartLongitude(transaksi.getStartLongitude());
            request.setEndLatitude(transaksi.getEndLatitude());
            request.setEndLongitude(transaksi.getEndLongitude());
            request.setJarak(transaksi.getJarak());
            request.setHarga(totalBiaya);
            request.setAdminFee(transaksi.getAdmin_fee());
            request.setWaktuOrder(transaksi.getWaktuOrder());
            request.setAlamatAsal(transaksi.getAlamatAsal());
            request.setAlamatTujuan(transaksi.getAlamatTujuan());
            request.setKodePromo(transaksi.getKodePromo());
            request.setKreditPromo(transaksi.getKreditPromo());
            request.setPakaiWallet(String.valueOf(transaksi.isPakaiWallet()));
            request.setEstimasi(transaksi.getEstimasi());
            request.setLayanan(layanan);
            request.setLayanandesc(layanandesk);
            request.setIcon(icon);
            request.setBiaya(binding.cost.getText().toString());
            request.setDistance(binding.distance.getText().toString());


            String namaLengkap = String.format("%s", loginUser.getFullnama());
            request.setNamaPelanggan(namaLengkap);
            request.setTelepon(loginUser.getNoTelepon());
            request.setType(ORDER);
        }
    }

    private void fcmBroadcast(int index, List<DriverModel> driverList) {
        DriverModel driverToSend = driverList.get(index);
        request.setTime_accept(new Date().getTime() + "");


        User loginUser = BaseApp.getInstance(this).getLoginUser();
        final NotificationService service = ServiceGenerator.createService(NotificationService.class, loginUser.getEmail(), loginUser.getPassword());

        RequestDriverJson.Data data = new RequestDriverJson.Data(
                request.getIdTransaksi(),
                request.getIdPelanggan(),
                request.getRegIdPelanggan(),
                request.getOrderFitur(),
                request.getHarga(),
                request.getWaktuOrder(),
                request.getAlamatAsal(),
                request.getAlamatTujuan(),
                request.isPakaiWallet(),
                request.getIcon(),
                request.getLayanan(),
                request.getLayanandesc(),
                request.getEstimasi(),
                request.getBiaya(),
                request.getAdminFee(),
                request.getDistance(),
                request.getTokenmerchant(),
                request.getIdtransmerchant(),
                request.getType()
        );

        RequestDriverJson requestDriverJson = new RequestDriverJson(driverToSend.getRegId(), data);

        service.sendNotification(requestDriverJson).enqueue(new Callback<DefaultResponseJson>() {
            @Override
            public void onResponse(Call<DefaultResponseJson> call, Response<DefaultResponseJson> response) {
                Log.d("response_23", response.message());
            }

            @Override
            public void onFailure(Call<DefaultResponseJson> call, Throwable t) {
                Log.d("response_23", t.getMessage());
            }
        });
    }

    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final DriverResponse response) {
        if (response.getResponse().equalsIgnoreCase(DriverResponse.ACCEPT) || response.getResponse().equals("3") || response.getResponse().equals("4")) {
            runOnUiThread(() -> {
                threadRun = false;
                for (DriverModel cDriver : driverAvailable) {
                    if (cDriver.getId().equals(response.getId())) {


                        Intent intent = new Intent(SendDetailActivity.this, ProgressActivity.class);
                        intent.putExtra("driver_id", cDriver.getId());
                        intent.putExtra("transaction_id", request.getIdTransaksi());
                        intent.putExtra("response", "2");
                        startActivity(intent);
                        DriverResponse response1 = new DriverResponse();
                        response1.setId("");
                        response1.setIdTransaksi("");
                        response1.setResponse("");
                        EventBus.getDefault().postSticky(response1);
                        finish();
                    }
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}
