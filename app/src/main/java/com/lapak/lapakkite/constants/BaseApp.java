package com.lapak.lapakkite.constants;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import androidx.multidex.MultiDex;

import com.lapak.lapakkite.utils.Log;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.lapak.lapakkite.models.FirebaseToken;
import com.lapak.lapakkite.models.User;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Ourdevelops Team on 10/13/2019.
 */

public class BaseApp extends Application {

    private static final int SCHEMA_VERSION = 0;

    private User loginUser = null;

    private Realm realmInstance;

    public static BaseApp getInstance(Context context) {
        return (BaseApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(SCHEMA_VERSION)
                .deleteRealmIfMigrationNeeded()
                .build();

        realmInstance = Realm.getDefaultInstance();
        start();

        FirebaseMessaging.getInstance().getToken().addOnSuccessListener(new OnSuccessListener<String>() {
            @Override
            public void onSuccess(String s) {
                FirebaseToken token = new FirebaseToken(s);
                FirebaseMessaging.getInstance().subscribeToTopic("ouride");
                FirebaseMessaging.getInstance().subscribeToTopic("customer").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                    }
                });
                Realm.setDefaultConfiguration(config);

//        realmInstance = Realm.getInstance(config);
                realmInstance.beginTransaction();
                realmInstance.delete(FirebaseToken.class);
                realmInstance.copyToRealm(token);
                realmInstance.commitTransaction();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public User getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(User loginUser) {
        this.loginUser = loginUser;
    }

    public final Realm getRealmInstance() {
        return realmInstance;
    }

    private void start() {
        Realm realm = getRealmInstance();
        User user = realm.where(User.class).findFirst();
        if (user != null) {
            setLoginUser(user);
        } else {
            Log.d("2504", "null");
        }
    }

}
