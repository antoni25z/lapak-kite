package com.lapak.lapakkite.item;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lapak.lapakkite.R;
import com.lapak.lapakkite.activity.DetailMerchantActivity;
import com.lapak.lapakkite.constants.BaseApp;
import com.lapak.lapakkite.constants.Constant;
import com.lapak.lapakkite.models.FavMerchantModel;
import com.lapak.lapakkite.models.User;
import com.lapak.lapakkite.utils.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

/**
 * Created by otacodes on 3/24/2019.
 */

public class MerchantFavItem extends RecyclerView.Adapter<MerchantFavItem.ItemRowHolder> {

    private List<FavMerchantModel> dataList;
    private Context mContext;
    private int rowLayout;
    private DatabaseHelper databaseHelper;


    public MerchantFavItem(Context context, List<FavMerchantModel> dataList, int rowLayout) {
        this.dataList = dataList;
        this.mContext = context;
        this.rowLayout = rowLayout;
        databaseHelper = new DatabaseHelper(mContext);
    }

    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemRowHolder holder, final int position) {
        final FavMerchantModel singleItem = dataList.get(position);
        holder.nameCategory.setText(singleItem.getNameMerchant());
        if (!singleItem.getImage_merchant().isEmpty()) {
            Picasso.get()
                    .load(Constant.IMAGESMERCHANT + singleItem.getImage_merchant())
                    .into(holder.images);
        }

        holder.content.setText(singleItem.getAddress_merchant());
        float km = Float.parseFloat(singleItem.getDistanceMerchant());
        String format = String.format(Locale.US, "%.1f", km);
        holder.distance.setText(format+"km");

        final User loginUser = BaseApp.getInstance(mContext).getLoginUser();
        holder.favourite.setOnClickListener(view -> {
            ContentValues fav = new ContentValues();
            if (databaseHelper.getFavMerchantById(String.valueOf(singleItem.getId_merchant()))) {
                databaseHelper.removeFavMerchantById(String.valueOf(singleItem.getId_merchant()));
                holder.favourite.setColorFilter(mContext.getResources().getColor(R.color.gray));
                Toast.makeText(mContext, "Remove To Favourite", Toast.LENGTH_SHORT).show();
            } else {
                fav.put(DatabaseHelper.KEY_ID_MERCHANT, singleItem.getId_merchant());
                fav.put(DatabaseHelper.KEY_USERID, loginUser.getId());
                fav.put(DatabaseHelper.KEY_NAME_MERCHANT, singleItem.getNameMerchant());
                fav.put(DatabaseHelper.KEY_ADDRESS_MERCHANT, singleItem.getAddress_merchant());
                fav.put(DatabaseHelper.KEY_DISTANCE_MERCHANT, singleItem.getDistanceMerchant());
                fav.put(DatabaseHelper.KEY_IMAGE_MERCHANT, singleItem.getImage_merchant());
                fav.put(DatabaseHelper.KEY_STATUS_PROMO, singleItem.getStatusPromo());
                fav.put(DatabaseHelper.KEY_LAT_MERCHANT, singleItem.getLatMerchant());
                fav.put(DatabaseHelper.KEY_LNG_MERCHANT, singleItem.getLngMerchant());
                databaseHelper.addFavMerchant(DatabaseHelper.TABLE_FAVOURITE_MERCHANT_NAME, fav, null);
                holder.favourite.setColorFilter(mContext.getResources().getColor(R.color.red));
                Toast.makeText(mContext, "Add To Favourite", Toast.LENGTH_SHORT).show();
            }

        });

        if (databaseHelper.getFavMerchantById(String.valueOf(singleItem.getId_merchant()))) {
            holder.favourite.setColorFilter(mContext.getResources().getColor(R.color.red));
        } else {
            holder.favourite.setColorFilter(mContext.getResources().getColor(R.color.gray));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, DetailMerchantActivity.class);
                i.putExtra("lat",singleItem.getLatMerchant());
                i.putExtra("lon",singleItem.getLngMerchant());
                i.putExtra("id",singleItem.getId_merchant());
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    static class ItemRowHolder extends RecyclerView.ViewHolder {
        TextView nameCategory, content, distance;
        ImageView images, favourite;

        ItemRowHolder(View itemView) {
            super(itemView);
            images = itemView.findViewById(R.id.image);
            nameCategory = itemView.findViewById(R.id.namakategori);
            content = itemView.findViewById(R.id.content);
            favourite = itemView.findViewById(R.id.favourite);
            distance = itemView.findViewById(R.id.distance);
        }
    }


}
