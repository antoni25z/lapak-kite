package com.lapak.lapakkite.item;


import com.lapak.lapakkite.models.ServiceDataModel;

public interface PermissionClick {
    void onClick(ServiceDataModel serviceDataModel, String mode);
}
