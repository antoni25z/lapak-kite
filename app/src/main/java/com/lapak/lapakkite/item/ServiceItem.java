package com.lapak.lapakkite.item;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lapak.lapakkite.R;
import com.lapak.lapakkite.constants.Constant;
import com.lapak.lapakkite.models.ServiceDataModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by otacodes on 3/24/2019.
 */

public class ServiceItem extends RecyclerView.Adapter<ServiceItem.ItemRowHolder> {

    private final List<ServiceDataModel> dataList;
    private final Context mContext;
    private final int rowLayout;
    private final OnItemClickListener listener;

    private PermissionClick click;


    public interface OnItemClickListener {
        void onItemClick(ServiceDataModel item);
    }

    public ServiceItem(final Context context, final List<ServiceDataModel> dataList, final int rowLayout,PermissionClick click, final OnItemClickListener listener) {
        this.dataList = dataList;
        mContext = context;
        this.rowLayout = rowLayout;
        this.listener = listener;
        this.click = click;
    }

    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(this.rowLayout, parent, false);
        return new ItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemRowHolder holder, int position) {

        final ServiceDataModel singleItem = dataList.get(position);
        holder.text.setText(singleItem.getFitur());

        Picasso.get().load(Constant.IMAGESFITUR + singleItem.getIcon()).into(holder.image);


        switch (singleItem.getHome()) {
            case "1":
                holder.background.setBackground(this.mContext.getResources().getDrawable(R.drawable.btn_rect));
                holder.background.setOnClickListener(v -> {
                    click.onClick(singleItem, "1");
                });
                break;
            case "2":
                holder.background.setBackground(this.mContext.getResources().getDrawable(R.drawable.btn_rect2));
                holder.background.setOnClickListener(v -> {
                    click.onClick(singleItem, "2");
                });
                break;
            case "3":
                holder.background.setBackground(mContext.getResources().getDrawable(R.drawable.btn_rect3, mContext.getTheme()));
                holder.background.setOnClickListener(view -> {
                    click.onClick(singleItem, "3");

                });
                break;
            case "4":
                holder.background.setBackground(this.mContext.getResources().getDrawable(R.drawable.btn_rect4, mContext.getTheme()));
                holder.background.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        click.onClick(singleItem, "4");
                    }
                });
                break;
        }
        if (singleItem.getHome().equals("0") && singleItem.getIdFitur() == 100) {
            holder.bind(singleItem, this.listener);
        }
    }

    @Override
    public int getItemCount() {
        return this.dataList.size();
    }

    class ItemRowHolder extends RecyclerView.ViewHolder {
        TextView text;
        ImageView background,image;

        ItemRowHolder(final View itemView) {
            super(itemView);
            this.background = itemView.findViewById(R.id.background);
            this.image = itemView.findViewById(R.id.image);
            this.text = itemView.findViewById(R.id.text);
        }

        public void bind(ServiceDataModel item, OnItemClickListener listener) {

            if (item.getHome().equals("0")) {
                this.background.getBackground().setColorFilter(ServiceItem.this.mContext.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_ATOP);
                    Picasso.get().load(R.drawable.ic_more)
                            .into(this.image);

                this.background.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            listener.onItemClick(item);
                        }
                    });
            }
        }
    }
}
