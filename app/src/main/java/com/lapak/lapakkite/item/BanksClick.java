package com.lapak.lapakkite.item;

import com.wensolution.wensxendit.AvailableBankModel;

public interface BanksClick {
    void onClick(AvailableBankModel availableBankModel);
}
