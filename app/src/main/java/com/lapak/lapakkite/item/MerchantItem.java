package com.lapak.lapakkite.item;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lapak.lapakkite.constants.BaseApp;
import com.lapak.lapakkite.models.User;
import com.lapak.lapakkite.utils.DatabaseHelper;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.lapak.lapakkite.R;
import com.lapak.lapakkite.activity.DetailMerchantActivity;
import com.lapak.lapakkite.constants.Constant;
import com.lapak.lapakkite.models.MerchantModel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by otacodes on 3/24/2019.
 */

public class MerchantItem extends RecyclerView.Adapter<MerchantItem.ItemRowHolder> {

    private List<MerchantModel> dataList;
    private Context mContext;
    private int rowLayout;
    private DatabaseHelper databaseHelper;

    public MerchantItem(Context context, List<MerchantModel> dataList, int rowLayout) {
        this.dataList = dataList;
        this.mContext = context;
        this.rowLayout = rowLayout;
        databaseHelper = new DatabaseHelper(context);
    }

    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ItemRowHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ItemRowHolder holder, final int position) {
        final MerchantModel singleItem = dataList.get(position);
        holder.name.setText(singleItem.getNama_merchant());
        if (!singleItem.getFoto_merchant().isEmpty()) {
            Picasso.get()
                    .load(Constant.IMAGESMERCHANT + singleItem.getFoto_merchant())
                    .into(holder.images);
        }

        if (singleItem.getStatus_promo().equals("1")) {
            holder.promobadge.setVisibility(View.VISIBLE);
            holder.shimmer.startShimmer();
        } else {
            holder.promobadge.setVisibility(View.GONE);
            holder.shimmer.stopShimmer();
        }

        holder.address.setText(singleItem.getAlamat_merchant());
        float km = Float.parseFloat(singleItem.getDistance());
        String format = String.format(Locale.US, "%.1f", km);
        holder.distance.setText(format+"km");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, DetailMerchantActivity.class);
                i.putExtra("lat",singleItem.getLatitude_merchant());
                i.putExtra("lon",singleItem.getLongitude_merchant());
                i.putExtra("id",singleItem.getId_merchant());
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mContext.startActivity(i);
            }
        });

        final User loginUser = BaseApp.getInstance(mContext).getLoginUser();
        holder.favourite.setOnClickListener(view -> {
            ContentValues fav = new ContentValues();
            if (databaseHelper.getFavMerchantById(String.valueOf(singleItem.getId_merchant()))) {
                databaseHelper.removeFavMerchantById(String.valueOf(singleItem.getId_merchant()));
                holder.favourite.setColorFilter(mContext.getResources().getColor(R.color.gray));
                Toast.makeText(mContext, "Remove To Favourite", Toast.LENGTH_SHORT).show();
            } else {
                fav.put(DatabaseHelper.KEY_ID_MERCHANT, singleItem.getId_merchant());
                fav.put(DatabaseHelper.KEY_USERID, loginUser.getId());
                fav.put(DatabaseHelper.KEY_NAME_MERCHANT, singleItem.getNama_merchant());
                fav.put(DatabaseHelper.KEY_ADDRESS_MERCHANT, singleItem.getAlamat_merchant());
                fav.put(DatabaseHelper.KEY_DISTANCE_MERCHANT, singleItem.getDistance());
                fav.put(DatabaseHelper.KEY_IMAGE_MERCHANT, singleItem.getFoto_merchant());
                fav.put(DatabaseHelper.KEY_STATUS_PROMO, singleItem.getStatus_promo());
                fav.put(DatabaseHelper.KEY_LAT_MERCHANT, singleItem.getLatitude_merchant());
                fav.put(DatabaseHelper.KEY_LNG_MERCHANT, singleItem.getLongitude_merchant());
                databaseHelper.addFavMerchant(DatabaseHelper.TABLE_FAVOURITE_MERCHANT_NAME, fav, null);
                holder.favourite.setColorFilter(mContext.getResources().getColor(R.color.red));
                Toast.makeText(mContext, "Add To Favourite", Toast.LENGTH_SHORT).show();
            }

        });

        if (databaseHelper.getFavMerchantById(String.valueOf(singleItem.getId_merchant()))) {
            holder.favourite.setColorFilter(mContext.getResources().getColor(R.color.red));
        } else {
            holder.favourite.setColorFilter(mContext.getResources().getColor(R.color.gray));
        }

    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    static class ItemRowHolder extends RecyclerView.ViewHolder {
        TextView name,address,distance;
        ImageView images, favourite;
        ShimmerFrameLayout shimmer;
        FrameLayout promobadge;

        ItemRowHolder(View itemView) {
            super(itemView);
            images = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.namakategori);
            shimmer = itemView.findViewById(R.id.shimreview);
            favourite = itemView.findViewById(R.id.favourite);
            promobadge = itemView.findViewById(R.id.promobadge);
            address = itemView.findViewById(R.id.content);
            distance = itemView.findViewById(R.id.distance);
        }
    }
}
