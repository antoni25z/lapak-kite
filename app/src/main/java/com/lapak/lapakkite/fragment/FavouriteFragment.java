package com.lapak.lapakkite.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.lapak.lapakkite.R;
import com.lapak.lapakkite.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;


public class FavouriteFragment extends Fragment {

    private ViewPager2 viewPager2;
    private TabLayout tabLayout;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View getView = inflater.inflate(R.layout.fragment_favorit, container, false);
        viewPager2 = getView.findViewById(R.id.tabviewpager);
        tabLayout = getView.findViewById(R.id.tabLayout);
        return getView;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String[] tabName = {"Berita", "Pelapak"};

        viewPager2.setAdapter(new ViewPagerAdapter(getParentFragmentManager(), getLifecycle()));

        new TabLayoutMediator(tabLayout, viewPager2, (tab, position) -> {
            tab.setText(tabName[position]);
        }).attach();
    }
}
