package com.lapak.lapakkite.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ourdevelops Team on 10/17/2019.
 */

public class FavMerchantModel extends RealmObject implements Serializable {

    @PrimaryKey
    @Expose
    @SerializedName("id_merchant")
    private String id_merchant;

    @Expose
    @SerializedName("name_merchant")
    private String nameMerchant;

    @Expose
    @SerializedName("distance_merchant")
    private String distanceMerchant;

    @Expose
    @SerializedName("image_merchant")
    private String image_merchant;

    @Expose
    @SerializedName("address_merchant")
    private String address_merchant;

    @Expose
    @SerializedName("status_promo")
    private String statusPromo;

    @Expose
    @SerializedName("userid")
    private String userid;

    @Expose
    @SerializedName("lat_merchant")
    private String latMerchant;

    @Expose
    @SerializedName("lng_merchant")
    private String lngMerchant;

    public String getLatMerchant() {
        return this.latMerchant;
    }

    public void setLatMerchant(final String latMerchant) {
        this.latMerchant = latMerchant;
    }

    public String getLngMerchant() {
        return this.lngMerchant;
    }

    public void setLngMerchant(final String lngMerchant) {
        this.lngMerchant = lngMerchant;
    }

    public String getId_merchant() {
        return this.id_merchant;
    }

    public void setId_merchant(final String id_merchant) {
        this.id_merchant = id_merchant;
    }

    public String getNameMerchant() {
        return this.nameMerchant;
    }

    public void setNameMerchant(final String nameMerchant) {
        this.nameMerchant = nameMerchant;
    }

    public String getDistanceMerchant() {
        return this.distanceMerchant;
    }

    public void setDistanceMerchant(final String distanceMerchant) {
        this.distanceMerchant = distanceMerchant;
    }

    public String getImage_merchant() {
        return this.image_merchant;
    }

    public void setImage_merchant(final String image_merchant) {
        this.image_merchant = image_merchant;
    }

    public String getAddress_merchant() {
        return this.address_merchant;
    }

    public void setAddress_merchant(final String address_merchant) {
        this.address_merchant = address_merchant;
    }

    public String getStatusPromo() {
        return this.statusPromo;
    }

    public void setStatusPromo(final String statusPromo) {
        this.statusPromo = statusPromo;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(final String userid) {
        this.userid = userid;
    }
}
